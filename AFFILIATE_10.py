from codecs import open
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import AFFILIATE_10
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/AFFILIATE_10_DATA.py", "w")
error_file = open("output/errors.py", 'a')

print('SCRAPING AFFILIATE 10 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)

degree_sign = '\N{DEGREE SIGN}'

AFFILIATE_10_DATA = {}
ERROR_DATA = {}

for network, site in AFFILIATE_10.items():
    try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_id('mrd-wx-forecast')
        days = forecast.find_elements_by_class_name('mrd-wx-item')
        starting_day = days[0].find_element_by_class_name(
            'mrd-wx-item-date').get_attribute('innerHTML').lower().strip()[:3]
        if starting_day != current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, current_day)

        network_code = network_codes[network]
        AFFILIATE_10_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_class_name(
            'mrd-wx-item-date').get_attribute('innerHTML').lower().strip()[:3]
            sky_code = day.find_element_by_class_name('mrd-wx-item-tease').get_attribute('innerHTML').strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_class_name(
                'mrd-left').get_attribute('innerHTML').strip().split(degree_sign)[0]
            low = day.find_element_by_class_name(
                'mrd-sub').get_attribute('innerHTML').split('/')[1].split(degree_sign)[0].strip()
            print(date, sky, high, low)
            if dayNumb == 1:
                AFFILIATE_10_DATA[network]['high'+ network_code + '[0]'] = str(high)
                AFFILIATE_10_DATA[network]['low'+ network_code + '[1]'] = str(low)
                AFFILIATE_10_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                AFFILIATE_10_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                AFFILIATE_10_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                AFFILIATE_10_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                AFFILIATE_10_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network) 
        ERROR_DATA[network] = {'error': 'could not fetch'} 

print('AFFILIATE_10_DATA = ', AFFILIATE_10_DATA, file=text_file)
print('AFFILIATE_10_ERRORS = ', ERROR_DATA, file=error_file)

browser.close()
browser.quit()
text_file.close()
