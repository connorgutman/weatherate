from codecs import open
from sys import platform
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import AFFILIATE_5
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if platform == "darwin":
    print('On Mac')
    browser = webdriver.Chrome(
        executable_path='/usr/local/bin/chromedriver', options=options)
    text_file = open("/Users/gwynn/Work/Weatherate/output/AFFILIATE_5_DATA.py", "w")
    error_file = open("/Users/gwynn/Work/Weatherate/output/errors.py", 'a')
else:
    print('On Linux')
    browser = webdriver.Chrome(options=options)
    text_file = open("output/AFFILIATE_5_DATA.py", "w")
    error_file = open("output/errors.py", 'a')

print('SCRAPING AFFILIATE 5 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
# AFFILIATE_5_current_date = current_date + timedelta(days=1)
# AFFILIATE_5_current_day = AFFILIATE_5_current_date.strftime("%a").lower()
# print('NOTE: AFFILIATE_5 displays the starting day differently, so we will be offseting to ', AFFILIATE_5_current_day)

degree_sign = '\N{DEGREE SIGN}'

AFFILIATE_5_DATA = {}
ERROR_DATA = {}

for network, site in AFFILIATE_5.items():
    try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_css_selector('.WeatherWeek-module_WeekContainer__kWqP')
        days = forecast.find_elements_by_class_name('WeatherDay-module_DayContainer__3C26')
        print(len(days))
        starting_day = days[0].find_element_by_class_name(
            'WeatherDay-module_text__3Xv1').get_attribute('innerHTML').strip().lower()[:3]
        if starting_day != current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, current_day)

        network_code = network_codes[network]
        AFFILIATE_5_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        # first_date = 'today'
        # first_high = days[0].find_element_by_xpath(
        #         '/html/body/div[1]/div/div/div[3]/div[1]/div/div/div/div/div[1]/div[1]/div/div[2]/div[1]/div[1]/span').get_attribute('innerHTML').split(degree_sign)[0]
        # first_low = days[0].find_element_by_xpath(
        #         '/html/body/div[1]/div/div/div[3]/div[1]/div/div/div/div/div[1]/div[1]/div/div[2]/div[1]/div[2]/span').get_attribute('innerHTML').split(degree_sign)[0]
        # first_sky_code = days[0].find_element_by_xpath(
        #         '/html/body/div[1]/div/div/div[3]/div[1]/div/div/div/div/div[1]/div[1]/div/div[1]/div[1]/span').get_attribute('innerHTML')
        # if first_sky_code in weather_codes:
        #     first_sky = weather_codes[first_sky_code]
        # else:
        #     first_sky = first_sky_code

        # print(first_date, first_high, first_low, first_sky)

        # AFFILIATE_5_DATA[network]['high'+ network_code + '[0]'] = str(first_high)
        # AFFILIATE_5_DATA[network]['low'+ network_code + '[1]'] = str(first_low)
        # AFFILIATE_5_DATA[network]['weather'+ network_code + '[0]'] = str(first_sky)
        # AFFILIATE_5_DATA[network]['weather'+ network_code + '[1]'] = '0'

        dayNumb = 1
        for day in days:
            date = day.find_element_by_class_name(
            'WeatherDay-module_text__3Xv1').get_attribute('innerHTML').strip().lower()[:3]
            sky_code = day.find_element_by_class_name('WeatherDay-module_conditionsData__2HVD').get_attribute('innerHTML')
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_class_name(
                'WeatherDay-module_highlow__3MKU').get_attribute('innerHTML').split(degree_sign)[0]
            low = day.find_element_by_class_name(
                'WeatherDay-module_low__3cHP').get_attribute('innerHTML').split(degree_sign)[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                AFFILIATE_5_DATA[network]['high'+ network_code + '[0]'] = str(high)
                AFFILIATE_5_DATA[network]['low'+ network_code + '[1]'] = str(low)
                AFFILIATE_5_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                AFFILIATE_5_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                AFFILIATE_5_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                AFFILIATE_5_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                AFFILIATE_5_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network) 
        ERROR_DATA[network] = {'error': 'could not fetch'}

print('AFFILIATE_5_DATA = ', AFFILIATE_5_DATA, file=text_file)
print('AFFILIATE_5_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
