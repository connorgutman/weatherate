from codecs import open
from sys import platform
import time
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import AFFILIATE_6
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if platform == "darwin":
    print('On Mac')
    browser = webdriver.Chrome(
        executable_path='/usr/local/bin/chromedriver', options=options)
    text_file = open("/Users/gwynn/Work/Weatherate/output/AFFILIATE_6_DATA.py", "w")
    error_file = open("/Users/gwynn/Work/Weatherate/output/errors.py", 'a')
else:
    print('On Linux')
    browser = webdriver.Chrome(options=options)
    text_file = open("output/AFFILIATE_6_DATA.py", "w")
    error_file = open("output/errors.py", 'a')

print('SCRAPING AFFILIATE 6 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
AFFILIATE_6_current_date = current_date + timedelta(days=2)
AFFILIATE_6_current_day = AFFILIATE_6_current_date.strftime("%a").lower()
print('NOTE: AFFILIATE_6 displays the starting day as TODAY, and the second day as TOMORROW so we will be offseting to ', AFFILIATE_6_current_day)

degree_sign= '\N{DEGREE SIGN}'

AFFILIATE_6_DATA = {}
ERROR_DATA = {}

for network, site in AFFILIATE_6.items():
    try:
        browser.get(site)
        time.sleep(5)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_class_name('weather-extended-items')
        days = forecast.find_elements_by_tag_name('li')
        print(len(days))

        starting_day = str(days[2].find_element_by_class_name('regular-label').get_attribute('innerHTML')).strip().lower()[:3]
        if starting_day != AFFILIATE_6_current_day:
            print(str(days[0].find_element_by_class_name('regular-label').get_attribute('innerHTML')).strip().lower()[:3])
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, AFFILIATE_6_current_day)
            

        network_code = network_codes[network]
        AFFILIATE_6_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        currentSky_code = days[0].find_element_by_class_name('body-sky-text').get_attribute('innerHTML').strip()
        if currentSky_code in weather_codes:
                currentSky = weather_codes[currentSky_code]
        else:
                currentSky = currentSky_code
        currentHigh = days[0].find_element_by_css_selector('.high .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
        currentLow = days[0].find_element_by_css_selector('.low .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
        print('today', currentSky, currentHigh, currentLow)

        nextSky_code = days[1].find_element_by_class_name('body-sky-text').get_attribute('innerHTML').strip()
        if nextSky_code in weather_codes:
                nextSky = weather_codes[nextSky_code]
        else:
                nextSky = nextSky_code
        nextHigh = days[1].find_element_by_css_selector('.high .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
        nextLow = days[1].find_element_by_css_selector('.low .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
        print('tomorrow', nextSky, nextHigh, nextLow)

        AFFILIATE_6_DATA[network]['high'+ network_code + '[0]'] = str(currentHigh)
        AFFILIATE_6_DATA[network]['low'+ network_code + '[1]'] = str(currentLow)
        AFFILIATE_6_DATA[network]['weather'+ network_code + '[0]'] = str(currentSky)
        AFFILIATE_6_DATA[network]['weather'+ network_code + '[1]'] = '0'

        AFFILIATE_6_DATA[network]['high'+ network_code + '[2]'] = str(nextHigh)
        AFFILIATE_6_DATA[network]['low'+ network_code + '[2]'] = str(nextLow)
        AFFILIATE_6_DATA[network]['weather'+ network_code + '[2]'] = str(nextSky)

        dayNumb = 1
        for day in days:
            if dayNumb > 2:
                date = day.find_element_by_css_selector('.regular-label:not(.rain)').text[:3].lower()
                sky_code = day.find_element_by_class_name(
                    'body-sky-text').get_attribute('innerHTML').strip()
                if sky_code in weather_codes:
                    sky = weather_codes[sky_code]
                else:
                    sky = sky_code
                high = day.find_element_by_css_selector('.high .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
                low = day.find_element_by_css_selector('.low .regular-label').get_attribute('innerHTML').strip().split(degree_sign)[0]
                if dayNumb > 2 and dayNumb < 5:
                    print(date, sky, high, low)
                    AFFILIATE_6_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                    AFFILIATE_6_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                    AFFILIATE_6_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
                    dayNumb = dayNumb + 1
                else:
                    print('Exceeded Day 4, skipping file entry.')
                    dayNumb = dayNumb + 1
            
            else:
                dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network) 
        ERROR_DATA[network] = {'error': 'could not fetch'}

print('AFFILIATE_6_DATA = ', AFFILIATE_6_DATA, file=text_file)
print('AFFILIATE_6_ERRORS = ', ERROR_DATA, file=error_file)

browser.close()
browser.quit()
text_file.close()
