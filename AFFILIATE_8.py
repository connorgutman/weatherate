from codecs import open
from network_codes import network_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import AFFILIATE_8
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/AFFILIATE_8_DATA.py", "w")
error_file = open("output/errors.py", 'a')

print('SCRAPING AFFILIATE 8 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)

degree_sign = '\N{DEGREE SIGN}'

weather_codes = {
    '65': '1',
    '66': '1',
    '67': '2',
    '68': '1',
    '69': '1',
    '70': '0',
    '71': '1',
    '72': '1',
    '73': '1',
    '74': '4',
    '75': '2',
    '76': '3',
    '77': '4',
    '78': '1,11',
    '79': '6',
    '80': '4',
    '81': '4',
    '82': '3',
    '83': '4',
    '84': '10',
    '85': '1',
    '86': '0',
    '87': '3',
    '88': '6',
    '89': '3',
    '90': '3',
    '91': '3',
    '92': '3',
    '93': '3',
    '94': '3',
    '95': '10',
    '96': '10',
    '97': '0',
    '98': '0',
    '99': '0'
}

AFFILIATE_8_DATA = {}
ERROR_DATA = {}

for network, site in AFFILIATE_8.items():
    try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecasts = browser.find_elements_by_css_selector('.hbi-main-weather-tab-table')
        forecast = forecasts[2]
        if network == 'KSTP':
            days = forecast.find_elements_by_tag_name('td')
        else:
            days = forecast.find_elements_by_tag_name('tr')
            
        starting_day = days[0].find_element_by_tag_name('span').get_attribute('innerHTML').strip().lower()[:3]
        print(starting_day)
        if starting_day != current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, current_day)

        network_code = network_codes[network]
        AFFILIATE_8_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_tag_name('span').get_attribute('innerHTML').strip().lower()[:3]
            sky_code = day.find_element_by_tag_name('img').get_attribute('src').split('wx_')[1].split('.')[0].strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = '0'
            high = day.find_element_by_css_selector(
                '.hbi-main-weather-tab-content div').get_attribute('innerHTML').split('Hi: ')[1].split('<s')[0].split(degree_sign)[0]
            low = day.find_element_by_css_selector(
                '.hbi-main-weather-tab-content div').get_attribute('innerHTML').split('Lo: ')[1].split('<s')[0].split(degree_sign)[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                AFFILIATE_8_DATA[network]['high'+ network_code + '[0]'] = str(high)
                AFFILIATE_8_DATA[network]['low'+ network_code + '[1]'] = str(low)
                AFFILIATE_8_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                AFFILIATE_8_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                AFFILIATE_8_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                AFFILIATE_8_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                AFFILIATE_8_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network) 
        ERROR_DATA[network] = {'error': 'could not fetch'}

print('AFFILIATE_8_DATA = ', AFFILIATE_8_DATA, file=text_file)
print('AFFILIATE_8_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
