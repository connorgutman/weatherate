import time
from codecs import open
from sys import platform
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import AFFILIATE_9
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if platform == "darwin":
    print('On Mac')
    browser = webdriver.Chrome(
        executable_path='/usr/local/bin/chromedriver', options=options)
    text_file = open("output/AFFILIATE_9_DATA.py", "w")
    error_file = open("output/errors.py", 'a')
else:
    print('On Linux')
    browser = webdriver.Chrome(options=options)
    text_file = open("output/AFFILIATE_9_DATA.py", "w")
    error_file = open("output/errors.py", 'a')

print('SCRAPING AFFILIATE 9 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
# AFFILIATE_9_current_date = current_date + timedelta(days=1)
# AFFILIATE_9_current_day = AFFILIATE_9_current_date.strftime("%a").lower()
# print 'NOTE: Affiliate 9 displays the starting day as TODAY, so we will be offseting to ', AFFILIATE_9_current_day

degree_sign = '\N{DEGREE SIGN}'

AFFILIATE_9_DATA = {}
ERROR_DATA = {}

for network, site in AFFILIATE_9.items():
    try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        time.sleep(5)
        forecast = browser.find_element_by_css_selector('wx-widget[days="10"]')
        days = forecast.find_elements_by_class_name('wx-daily-forecast-balgnj-DayContainer')
        starting_day = days[0].find_element_by_tag_name(
            'h3').get_attribute('innerHTML').lower().strip()[:3]
        if starting_day != current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, current_day)

        network_code = network_codes[network]
        AFFILIATE_9_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_tag_name(
            'h3').get_attribute('innerHTML').lower().strip()[:3]
            sky_code = day.find_element_by_tag_name('img').get_attribute('alt').strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_class_name(
                'wxmap--src-widgets-daily-components-day-cells-temp-cell__day').get_attribute('innerHTML').strip().split(degree_sign)[0]
            low = day.find_element_by_class_name(
                'wxmap--src-widgets-daily-components-day-cells-temp-cell__night').get_attribute('innerHTML').strip().split(degree_sign)[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                AFFILIATE_9_DATA[network]['high'+ network_code + '[0]'] = high.encode('utf-8')
                AFFILIATE_9_DATA[network]['low'+ network_code + '[1]'] = low.encode('utf-8')
                AFFILIATE_9_DATA[network]['weather'+ network_code + '[0]'] = sky.encode('utf-8')
                AFFILIATE_9_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                AFFILIATE_9_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = high.encode('utf-8')
                AFFILIATE_9_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = low.encode('utf-8')
                AFFILIATE_9_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = sky.encode('utf-8')
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network)
        ERROR_DATA[network] = {'error': 'could not fetch'} 

print('AFFILIATE_9_DATA = ', AFFILIATE_9_DATA, file=text_file)
print('AFFILIATE_9_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
