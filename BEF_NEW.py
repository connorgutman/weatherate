from codecs import open
from selenium import webdriver
from sites import networks, AFFILIATE_1, AFFILIATE_2, AFFILIATE_3, AFFILIATE_4, AFFILIATE_5, AFFILIATE_6, AFFILIATE_7, AFFILIATE_8, AFFILIATE_9, AFFILIATE_10, AFFILIATE_11, AFFILIATE_13, AFFILIATE_16, AFFILIATE_17, AFFILIATE_19, AFFILIATE_20, AFFILIATE_28, AFFILIATE_OTHER, ABC, ACCUWEATHER, CBS, CW, FOX, NBC, KSL, ESPANOL_NBC, ESPANOL_1
from standard_data import new_entry, selectors
from standard_functions import grab, strip, checkDate, recordEntry, checkDict, offset

"Initiate output dicts and open respective files"
BEF_DATA = {}
ERROR_DATA = {}
BEF_FILE = open("/home/gwynn/Work/Weatherate/output/BEF_DATA.py", "w")
ERROR_FILE = open("/home/gwynn/Work/Weatherate/output/errors.py", 'a')

"Configure Selenium"
options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
browser = webdriver.Chrome(
    executable_path='/usr/bin/chromedriver', options=options)

"Begin cycling through networks"
"for network, site in ABC.items(), CBS.items(), etc"
for network, site in AFFILIATE_11.items():
    # try:
        "Initialize data & configure selectors"
        print(network + ' Forecasts:')
        BEF_DATA[network] = new_entry
        sel = selectors[networks[network]]
        "Load site"
        browser.get(site)
        "Isolate forecasts"
        forecast = grab(sel['forecast'], sel['f_sel'], browser)
        days = grab(sel['days'], sel['d_sel'], forecast)
        "Make sure site is up-to-date"
        checkDate(days, sel)
        dayNumb = 1
        for day in days:
            "Fetch forecast data"
            high = grab(sel['high'], sel['h_sel'], day).get_attribute(
                sel['h_attr'])
            low = grab(sel['low'], sel['l_sel'], day).get_attribute(
                sel['l_attr'])
            sky_data = grab(sel['skyCode'], sel['s_sel'], day).get_attribute(
                sel['s_attr'])

            "Format forecast data"
            h = strip(networks[network], 'high', high)
            l = strip(networks[network], 'low', low)
            sky = strip(networks[network], 'sky', sky_data)
            
            "Check dictionary for sky forecast"
            s = checkDict(sky)

            "Record forecast data"
            recordEntry(BEF_DATA, network, dayNumb, s, h, l)

            "Move to next day"
            dayNumb = dayNumb + 1
    # except:
        # print('Could not fetch site ' + network)
        # ERROR_DATA[network] = {'error': 'could not fetch'}

"Save output and quit"
print('BEF_DATA = ', BEF_DATA, file=BEF_FILE)
print('BEF_ERRORS = ', ERROR_DATA, file=ERROR_FILE)
browser.close()
browser.quit()
BEF_FILE.close()
ERROR_FILE.close()
