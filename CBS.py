from codecs import open
import time
from sys import platform
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import CBS
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if platform == "darwin":
    print('On Mac')
    browser = webdriver.Chrome(
        executable_path='/usr/local/bin/chromedriver', options=options)
    text_file = open("/Users/gwynn/Work/Weatherate/output/CBS_DATA.py", "w")
    error_file = open("/Users/gwynn/Work/Weatherate/output/errors.py", 'a')
else:
    print('On Linux')
    browser = webdriver.Chrome(options=options)
    text_file = open("output/CBS_DATA.py", "w")
    error_file = open("output/errors.py", 'a')

print('SCRAPING CBS =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)

degree_sign = '\N{DEGREE SIGN}'

CBS_DATA = {}
ERROR_DATA = {}

for network, site in CBS.items():
    try:
        browser.get(site)
        time.sleep(5)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_class_name('wxmap--src-widgets-daily-components-days-days__table')
        days = forecast.find_elements_by_class_name('wx-daily-forecast-balgnj-DayContainer')
        print(len(days))

        network_code = network_codes[network]
        CBS_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_tag_name('h3').text[:3].lower()
            sky_code = day.find_element_by_class_name(
                'wxmap--src-widgets-daily-components-day-cells-conditions-cell__root').get_attribute('innerHTML').strip().split('>')[1]
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_class_name(
                'wxmap--src-widgets-daily-components-day-cells-temp-cell__day').get_attribute('innerHTML').strip().split(degree_sign)[0]
            low = day.find_element_by_class_name(
                'wxmap--src-widgets-daily-components-day-cells-temp-cell__night').get_attribute('innerHTML').strip().split(degree_sign)[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                CBS_DATA[network]['high'+ network_code + '[0]'] = str(high)
                CBS_DATA[network]['low'+ network_code + '[1]'] = str(low)
                CBS_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                CBS_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                CBS_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                CBS_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                CBS_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network)
        ERROR_DATA[network] = {'error': 'could not fetch'}

print('CBS_DATA = ', CBS_DATA, file=text_file)
print('CBS_ERRORS = ', ERROR_DATA, file=error_file)

browser.close()
browser.quit()
text_file.close()
