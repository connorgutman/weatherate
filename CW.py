from codecs import open
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import CW
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/CW_DATA.py", "w")
error_file = open("output/errors.py", 'a')

print('SCRAPING CW =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
cw_current_date = current_date + timedelta(days=1)
cw_current_day = cw_current_date.strftime("%a").lower()
print('NOTE: CW displays the starting day as TODAY, so we will be offseting to ', cw_current_day)

degree_sign= '\N{DEGREE SIGN}'

CW_DATA = {}
ERROR_DATA = {}

for network, site in CW.items():
    # try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        currentSky_code = browser.find_element_by_css_selector('.currently i').get_attribute('className').strip()
        if currentSky_code in weather_codes:
                currentSky = weather_codes[currentSky_code]
        else:
                currentSky = currentSky_code
        currentHigh = browser.find_element_by_css_selector('.hi-low strong:nth-of-type(2)').get_attribute('innerHTML').strip().split(degree_sign)[0]
        currentLow = browser.find_element_by_css_selector('.hi-low strong:nth-of-type(1)').get_attribute('innerHTML').strip().split(degree_sign)[0]
        print('today', currentSky, currentHigh, currentLow)
        # print >> text_file, network, 'today', currentSky, currentHigh, currentLow 
        forecast = browser.find_element_by_class_name('forecast')
        days = forecast.find_elements_by_class_name('span-day')

        starting_day = days[0].find_element_by_class_name('date').text[:3].lower()
        if starting_day != cw_current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, cw_current_day)

        network_code = network_codes[network]
        CW_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        CW_DATA[network]['high'+ network_code + '[0]'] = str(currentHigh)
        CW_DATA[network]['low'+ network_code + '[1]'] = str(currentLow)
        CW_DATA[network]['weather'+ network_code + '[0]'] = str(currentSky)
        CW_DATA[network]['weather'+ network_code + '[1]'] = '0'

        dayNumb = 2
        for day in days:
            date = day.find_element_by_class_name('date').text[:3].lower()
            sky_code = day.find_element_by_class_name(
                'day-description').get_attribute('innerHTML').strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            temps = day.find_element_by_class_name(
                'day-hi-low').get_attribute('innerHTML').strip()
            high = temps.split('i>')[1].split(degree_sign)[0].split('\n\t\t\t\t\t\t\t\t\t\t\t\t')[1].strip()
            low = temps.split(degree_sign)[0]

            print(date, sky, high, low)
            if dayNumb < 5:
                CW_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                CW_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                CW_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    # except:
        # print 'Could not fetch site ' + network 
        # ERROR_DATA[network] = {'error': 'could not fetch'}

print('CW_DATA = ', CW_DATA, file=text_file)
print('CW_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
