# -*- coding: utf-8 -*-
from codecs import open
import time
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import ESPANOL_1
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/ESPANOL_1_DATA.py", "w")
error_file = open("output/errors.py", 'a')

print('SCRAPING ESPANOL 1 =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
ESPANOL_1_current_date = current_date + timedelta(days=1)
ESPANOL_1_current_day = ESPANOL_1_current_date.strftime("%a").lower()
ESPANOL_1_current_day_num = ESPANOL_1_current_date.day
print('NOTE: ESPANOL 1 displays the starting day as TODAY/HOY, so we will be offseting to ', ESPANOL_1_current_day_num)

week_days = {
    'lun': 'mon',
    'mar': 'tue',
    'mié': 'wed',
    'jue': 'thu',
    'vie': 'fri',
    'sáb': 'sat',
    'dom': 'sun'
}

degree_sign= '\N{DEGREE SIGN}'

ESPANOL_1_DATA = {}
ERROR_DATA = {}

for network, site in ESPANOL_1.items():
    try:
        browser.get(site)
        time.sleep(5)
        print('Fetched site...')
        
        # expandBTN=''
        # while not expandBTN:
            # try:
                # expandBTN = browser.find_element_by_class_name('LayerNavigation__BottomBarContainer-sc-45ivwo-2')
                # expandBTN.click()
                # dailyBTN = browser.find_elements_by_class_name('LayerNavigation__Option-sc-45ivwo-7')[1]
                # dailyBTN.click()
                # print 'finding...'

            # except:continue
                    
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_class_name('sc-1niogcj-1')
        days = forecast.find_elements_by_class_name('ForecastRow__Wrapper-sc-1gbqgyc-0')
        print(('days: ' , len(days)))
        # print days
        starting_day_espanol = days[0].find_element_by_css_selector('.eNwRdi').get_attribute('innerHTML').lower().split(' ')[0]
        # print starting_day_espanol
        # starting_day = week_days[starting_day_espanol]
        if starting_day_espanol != str(ESPANOL_1_current_day_num):
                print('Website is not up to date. First day on record is ', starting_day_espanol)
                offset(days, starting_day_espanol, ESPANOL_1_current_day)
        else:
            print('starting day matches!')

        day_1_high = browser.find_elements_by_class_name('sc-1p6dg5w-0')[0].get_attribute('innerHTML').split('\xba')[0]
        day_1_low = browser.find_elements_by_class_name('sc-1p6dg5w-0')[1].get_attribute('innerHTML').split('\xba')[0]
        day_1_sky_code = browser.find_element_by_class_name('sc-t0r9wg-6').get_attribute('innerHTML')
        if day_1_sky_code in weather_codes:
            day_1_sky = weather_codes[day_1_sky_code]
        else:
            day_1_sky = day_1_sky_code


        print('Day 1 is: ', day_1_sky, day_1_high, day_1_low)

        network_code = network_codes[network]
        ESPANOL_1_DATA[network] = {
            'high'+ network_code + '[0]': day_1_high,
            'low'+ network_code + '[1]': day_1_low,
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': day_1_sky,
            'weather'+ network_code + '[1]': '0',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 2
        for day in days:
            date = day.find_element_by_css_selector('.eNwRdi').get_attribute('innerHTML').lower().split(' ')[0]
            sky_code = day.find_element_by_css_selector('.bynto p').get_attribute('innerHTML').lower().strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_elements_by_css_selector(
                '.ForecastRow__TempValue-sc-1gbqgyc-1')[0].get_attribute('innerHTML').split('\xba')[0].split('<')[0]
            low = day.find_elements_by_css_selector(
                '.ForecastRow__TempValue-sc-1gbqgyc-1')[1].get_attribute('innerHTML').split('\xba')[0].split('<')[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                ESPANOL_1_DATA[network]['high'+ network_code + '[0]'] = high
                ESPANOL_1_DATA[network]['low'+ network_code + '[1]'] = low
                ESPANOL_1_DATA[network]['weather'+ network_code + '[0]'] = sky
                ESPANOL_1_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                ESPANOL_1_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = high
                ESPANOL_1_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = low
                ESPANOL_1_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = sky
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1

        print(ESPANOL_1_DATA[network])
    except:
        print('Could not fetch site ' + network)
        ERROR_DATA[network] = {'error': 'could not fetch'}


print('ESPANOL_1_DATA = ', ESPANOL_1_DATA, file=text_file)
print('ESPANOL_1_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
