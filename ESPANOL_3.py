from codecs import open
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import ESPANOL_3
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/ESPANOL_3_DATA.py", "w")

print('SCRAPING ESPANOL 3 =======================================')

ESPANOL_3_DATA = {}

for network, site in ESPANOL_3.items():
    
    network_code = network_codes[network]
    ESPANOL_3_DATA[network] = {
        'high'+ network_code + '[0]': '999',
        'low'+ network_code + '[1]': '-999',
        'high'+ network_code + '[2]': '999',
        'low'+ network_code + '[2]': '-999',
        'high'+ network_code + '[3]': '999',
        'low'+ network_code + '[3]': '-999',
        'high'+ network_code + '[4]': '999',
        'low'+ network_code + '[4]': '-999',
        'weather'+ network_code + '[0]': '0',
        'weather'+ network_code + '[1]': '0',
        'weather'+ network_code + '[2]': '0',
        'weather'+ network_code + '[3]': '0',
        'weather'+ network_code + '[4]': '0'
    }

print('ESPANOL_3_DATA = ', ESPANOL_3_DATA, file=text_file)
browser.close()
browser.quit()
text_file.close()
