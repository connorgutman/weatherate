# -*- coding: utf-8 -*-
from codecs import open
import time
from sys import platform
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import ESPANOL_NBC
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

if platform == "darwin":
    print('On Mac')
    browser = webdriver.Chrome(
        executable_path='/usr/local/bin/chromedriver', options=options)
    text_file = open("/Users/gwynn/Work/Weatherate/output/ESPANOL_NBC_DATA.py", "w")
    error_file = open("/Users/gwynn/Work/Weatherate/output/errors.py", 'a')
else:
    print('On Linux')
    browser = webdriver.Chrome(options=options)
    text_file = open("output/ESPANOL_NBC_DATA.py", "w")
    error_file = open("output/errors.py", 'a')



print('SCRAPING ESPANOL NBC =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
ESPANOL_NBC_current_date = current_date + timedelta(days=1)
ESPANOL_NBC_current_day = ESPANOL_NBC_current_date.strftime("%a").lower()
print('NOTE: ESPANOL NBC displays the starting day as TODAY/HOY, so we will be offseting to ', ESPANOL_NBC_current_day)

current_folder_date = current_date + timedelta(days=-1)
current_folder = current_folder_date.strftime("%m/%d")
print('Current directory: ' + current_folder)

degree_sign= '\N{DEGREE SIGN}'

week_days = {
    'lun': 'mon',
    'mar': 'tue',
    'mié': 'wed',
    'jue': 'thu',
    'vie': 'fri',
    'sáb': 'sat',
    'dom': 'sun'
}

ESPANOL_NBC_DATA = {}
ERROR_DATA = {}

for network, site in ESPANOL_NBC.items():
    try:
        browser.get(site)
        time.sleep(5)
        DOM = browser.page_source
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_class_name('tenDay__wrapper')
        days = forecast.find_elements_by_class_name('tenDay__day')
        print(len(days))
        starting_day_espanol = days[1].find_element_by_class_name('tenDay__day-label').get_attribute('innerHTML').lower().strip()
        print(starting_day_espanol)
        starting_day = week_days[starting_day_espanol]
        if starting_day != ESPANOL_NBC_current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, ESPANOL_NBC_current_day)

        network_code = network_codes[network]
        ESPANOL_NBC_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_class_name('tenDay__day-label').get_attribute('innerHTML').strip().lower()
            sky_code = day.find_element_by_class_name(
                'tenDay__sky-text').get_attribute('innerHTML').strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_css_selector(
                '.tenDay__temp-hi').get_attribute('innerHTML').strip().split('º')[0].split('</span>')[1]
            low = day.find_element_by_css_selector(
                '.tenDay__temp-lo').get_attribute('innerHTML').strip().split('º')[0].split('</span>')[1]
            print(date, sky, high, low)
            if dayNumb == 1:
                ESPANOL_NBC_DATA[network]['high'+ network_code + '[0]'] = high
                ESPANOL_NBC_DATA[network]['low'+ network_code + '[1]'] = low
                ESPANOL_NBC_DATA[network]['weather'+ network_code + '[0]'] = sky
                ESPANOL_NBC_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                ESPANOL_NBC_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = high
                ESPANOL_NBC_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = low
                ESPANOL_NBC_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = sky
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network)
        ERROR_DATA[network] = {'error': 'could not fetch'}

print('ESPANOL_NBC_DATA = ', ESPANOL_NBC_DATA, file=text_file)
print('ESPANOL_NBC_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
