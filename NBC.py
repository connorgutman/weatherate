import time
from codecs import open
from network_codes import network_codes
from weather_codes import weather_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import NBC
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')

browser = webdriver.Chrome(options=options)

text_file = open("output/NBC_DATA.py", "w")
error_file = open("output/errors.py", 'a')

print('SCRAPING NBC =======================================')

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print('Today is: ', current_day)
NBC_current_date = current_date + timedelta(days=1)
NBC_current_day = NBC_current_date.strftime("%a").lower()
print('NOTE: NBC displays the starting day as TODAY, so we will be offseting to ', NBC_current_day)

degree_sign= '\N{DEGREE SIGN}'

NBC_DATA = {}
ERROR_DATA = {}

for network, site in NBC.items():
    try:
        browser.get(site)
        time.sleep(5)
        DOM = browser.page_source.encode('utf-8')
        print((network + ' Forecasts:'))
        forecast = browser.find_element_by_class_name('tenDay__wrapper')
        days = forecast.find_elements_by_class_name('tenDay__day')

        starting_day = days[1].find_element_by_class_name('tenDay__day-label').get_attribute('innerHTML').lower().strip()
        if starting_day != NBC_current_day:
            print('Website is not up to date. First day on record is ', starting_day)
            offset(days, starting_day, NBC_current_day)

        network_code = network_codes[network]
        NBC_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_class_name('tenDay__day-label').get_attribute('innerHTML').strip().lower()
            sky_code = day.find_element_by_class_name(
                'tenDay__sky-text').get_attribute('innerHTML').strip()
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_css_selector(
                '.tenDay__temp-hi').get_attribute('innerHTML').strip().split('/span>')[1].split('º')[0]
            low = day.find_element_by_css_selector(
                '.tenDay__temp-lo').get_attribute('innerHTML').strip().split('/span>')[1].split('º')[0]
            print(date, sky, high, low)
            if dayNumb == 1:
                NBC_DATA[network]['high'+ network_code + '[0]'] = str(high)
                NBC_DATA[network]['low'+ network_code + '[1]'] = str(low)
                NBC_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                NBC_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                NBC_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                NBC_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                NBC_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print('Exceeded Day 4, skipping file entry.')
            dayNumb = dayNumb + 1
    except:
        print('Could not fetch site ' + network)
        ERROR_DATA[network] = {'error': 'could not fetch'}
        
print('NBC_DATA = ', NBC_DATA, file=text_file)
print('NBC_ERRORS = ', ERROR_DATA, file=error_file)
browser.close()
browser.quit()
text_file.close()
