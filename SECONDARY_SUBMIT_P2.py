from codecs import open
from datetime import datetime, time, timedelta
import random
from selenium import webdriver

# from network_codes import network_codes
from city_codes import city_codes

from schedule import SCHEDULE

TODAY = datetime.combine(datetime.today(), time.min)
YESTERDAY = (TODAY - datetime(1970,1,2)).total_seconds()
YDATE = datetime.today() - timedelta(days=1)
DATE_URL = YDATE.strftime("%Y/%-m/%-d")
# DATE_URL = datetime.today().strftime("%Y/%-m/%-d")
TEMP_DATE_URL = TODAY.strftime("%Y/%-m/%-d")

print DATE_URL

print "Today's schedule: " + SCHEDULE[TODAY.strftime('%m.%d.%Y')]

FORECAST_DATA = {}
FORECAST_SUMS = {}
FORECAST_AVERAGES = {}

DUMMY_DATA = {}

CITIES = []

options = webdriver.ChromeOptions()
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
# options.add_argument('headless')


browser = webdriver.Chrome(chrome_options=options)

text_file = open("/home/ber/Work/Weatherate/output/SECONDARY_SUBMIT_DATA.py", "w")
text_file2 = open("/home/ber/Work/Weatherate/output/SECONDARY_SUBMIT_DATA_SUMS.py", "w")

for network, city in city_codes.iteritems():
    FORECAST_DATA[city] = {}
    FORECAST_SUMS[city] = {
        'networkCount': '0',
        'high[0]': '0',
        'low[1]': '0',
        'high[2]': '0',
        'low[2]': '0',
        'high[3]': '0',
        'low[3]': '0',
        'high[4]': '0',
        'low[4]': '0',
        'weather[0]': '',
        'weather[1]': '',
        'weather[2]': '',
        'weather[3]': '',
        'weather[4]': ''
    }

print FORECAST_DATA

browser.get(
    'http://weatherate.com/forecast_bulk_entry/1/' + str(DATE_URL))

# Sign in to admin portal
unameBox = browser.find_element_by_name('username')
passBox = browser.find_element_by_name('password')
submitBox = browser.find_element_by_xpath("//button[@type='submit']")
unameBox.send_keys('connorg')
passBox.send_keys('TokyoGenso21')
submitBox.click()
raw_input("Press Enter to continue...")
# Get latest form names
stationNums = {}
for x in range(0, 373):
    ename = "form-" + str(x) + "-station_name"
    stationNameBox = browser.find_element_by_name(ename)
    stationNums[str(stationNameBox.get_attribute("value"))] = str(x)
    print("Station " + str(x) + " is " + stationNameBox.get_attribute("value"))


for network, city in city_codes.iteritems():
    # form-1-d1_high
    try:
        formNum = stationNums[network]
        elemPrefix = 'form-' + str(formNum) + '-'
        box = browser.find_element_by_name(elemPrefix + 'd1_high')
        if not box.get_attribute("value"):
            print 'Cell does not contain text'
        else:
            print 'Cell already containted text, scraping...'
            FORECAST_DATA[city][network] = {
                'd1_high': browser.find_element_by_name(elemPrefix + 'd1_high').get_attribute("value"),
                'd1n_low': browser.find_element_by_name(elemPrefix + 'd1n_low').get_attribute("value"),
                'd2_high': browser.find_element_by_name(elemPrefix + 'd2_high').get_attribute("value"),
                'd2_low': browser.find_element_by_name(elemPrefix + 'd2_low').get_attribute("value"),
                'd3_high': browser.find_element_by_name(elemPrefix + 'd3_high').get_attribute("value"),
                'd3_low': browser.find_element_by_name(elemPrefix + 'd3_low').get_attribute("value"),
                'd4_high': browser.find_element_by_name(elemPrefix + 'd4_high').get_attribute("value"),
                'd4_low': browser.find_element_by_name(elemPrefix + 'd4_low').get_attribute("value"),
                'd1': browser.find_element_by_name(elemPrefix + 'd1').get_attribute("value"),
                'd1n': browser.find_element_by_name(elemPrefix + 'd1n').get_attribute("value"),
                'd2': browser.find_element_by_name(elemPrefix + 'd2').get_attribute("value"),
                'd3': browser.find_element_by_name(elemPrefix + 'd3').get_attribute("value"),
                'd4': browser.find_element_by_name(elemPrefix + 'd4').get_attribute("value")
            }

            FORECAST_SUMS[city] = {
                'networkCount': int(FORECAST_SUMS[city]['networkCount']) + 1,
                'high[0]': int(FORECAST_SUMS[city]['high[0]']) + int(browser.find_element_by_name(elemPrefix + 'd1_high').get_attribute("value")),
                'low[1]': int(FORECAST_SUMS[city]['low[1]']) + int(browser.find_element_by_name(elemPrefix + 'd1n_low').get_attribute("value")),
                'high[2]': int(FORECAST_SUMS[city]['high[2]']) + int(browser.find_element_by_name(elemPrefix + 'd2_high').get_attribute("value")),
                'low[2]': int(FORECAST_SUMS[city]['low[2]']) + int(browser.find_element_by_name(elemPrefix + 'd2_low').get_attribute("value")),
                'high[3]': int(FORECAST_SUMS[city]['high[3]']) + int(browser.find_element_by_name(elemPrefix + 'd3_high').get_attribute("value")),
                'low[3]': int(FORECAST_SUMS[city]['low[3]']) + int(browser.find_element_by_name(elemPrefix + 'd3_low').get_attribute("value")),
                'high[4]': int(FORECAST_SUMS[city]['high[4]']) + int(browser.find_element_by_name(elemPrefix + 'd4_high').get_attribute("value")),
                'low[4]': int(FORECAST_SUMS[city]['low[4]']) + int(browser.find_element_by_name(elemPrefix + 'd4_low').get_attribute("value")),
                'weather[0]': browser.find_element_by_name(elemPrefix + 'd1').get_attribute("value"),
                'weather[1]': browser.find_element_by_name(elemPrefix + 'd1n').get_attribute("value"),
                'weather[2]': browser.find_element_by_name(elemPrefix + 'd2').get_attribute("value"),
                'weather[3]': browser.find_element_by_name(elemPrefix + 'd3').get_attribute("value"),
                'weather[4]': browser.find_element_by_name(elemPrefix + 'd4').get_attribute("value")
            }
    except:
        print 'could not average market.'

for network, city in city_codes.iteritems():
    # form-1-d1_high
    try:
        formNum = stationNums[network]
        elemPrefix = 'form-' + str(formNum) + '-'
        box = browser.find_element_by_name(elemPrefix + 'd1_high')
        if not box.get_attribute("value"):
            if city is not 'SF':
                if city is not 'SANDIEGO':
                    print network + ' cell does not contain text, entering average...'
                    browser.find_element_by_name(elemPrefix + 'd1_high').send_keys((FORECAST_SUMS[city]['high[0]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd1n_low').send_keys((FORECAST_SUMS[city]['low[1]'] / FORECAST_SUMS[city]['networkCount'] + random.randrange(-1, 1)))
                    browser.find_element_by_name(elemPrefix + 'd2_high').send_keys((FORECAST_SUMS[city]['high[2]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd2_low').send_keys((FORECAST_SUMS[city]['low[2]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd3_high').send_keys((FORECAST_SUMS[city]['high[3]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd3_low').send_keys((FORECAST_SUMS[city]['low[3]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd4_high').send_keys((FORECAST_SUMS[city]['high[4]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))
                    browser.find_element_by_name(elemPrefix + 'd4_low').send_keys((FORECAST_SUMS[city]['low[4]'] / FORECAST_SUMS[city]['networkCount']) + random.randrange(-1, 1))

                    browser.find_element_by_name(elemPrefix + 'd1').send_keys(FORECAST_SUMS[city]['weather[0]'])
                    browser.find_element_by_name(elemPrefix + 'd1n').send_keys(FORECAST_SUMS[city]['weather[1]'])
                    browser.find_element_by_name(elemPrefix + 'd2').send_keys(FORECAST_SUMS[city]['weather[2]'])
                    browser.find_element_by_name(elemPrefix + 'd3').send_keys(FORECAST_SUMS[city]['weather[3]'])
                    browser.find_element_by_name(elemPrefix + 'd4').send_keys(FORECAST_SUMS[city]['weather[4]'])
                else:
                    print 'skipping SAN DIEGO'
            else:
                print 'skipping SF'


        else:
            print 'Cell already containted text, skipping...'
    except:
        print 'could not calculate average or city removed.'
# Check for high/low errors

randomErrors = {}

for network, city in city_codes.iteritems():
    # form-1-d1_high
    try:
        formNum = stationNums[network]
        elemPrefix = 'form-' + str(formNum) + '-'
        box = browser.find_element_by_name(elemPrefix + 'd1_high')
        if box.get_attribute("value"):
            if city is not 'SF':
                if city is not 'SANDIEGO':
                    print network + ' is being checked...'
                    h1 = int(browser.find_element_by_name(elemPrefix + 'd1_high').get_attribute("value"))
                    l1 = int(browser.find_element_by_name(elemPrefix + 'd1n_low').get_attribute("value"))
                    h2 = int(browser.find_element_by_name(elemPrefix + 'd2_high').get_attribute("value"))
                    l2 = int(browser.find_element_by_name(elemPrefix + 'd2_low').get_attribute("value"))
                    h3 = int(browser.find_element_by_name(elemPrefix + 'd3_high').get_attribute("value"))
                    l3 = int(browser.find_element_by_name(elemPrefix + 'd3_low').get_attribute("value"))
                    h4 = int(browser.find_element_by_name(elemPrefix + 'd4_high').get_attribute("value"))
                    l4 = int(browser.find_element_by_name(elemPrefix + 'd4_low').get_attribute("value"))
                    if l1 > h1:
                        randomErrors[network] = 'd1 error'
                    elif l2 > h2:
                        randomErrors[network] = 'd2 error'
                    elif l3 > h3:
                        randomErrors[network] = 'd3 error'
                    elif l4 > h4:
                        randomErrors[network] = 'd4 error'
                    else:
                        print 'no errors for this network.'
                else:
                    print 'skipping SAN DIEGO'
            else:
                print 'skipping SF'


        else:
            print 'Cell is empty, skipping...'
    except:
        print 'Could not check answer or city deleted.'
print randomErrors

if SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'BEF':
    raw_input("Press Enter to continue...")
    print 'Submitting...'
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'BEF OWDES':
    print 'Submitting...'
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    # submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'OWDES BEF':
    print 'Submitting...'
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    # submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'OWDES':
    print 'Today is not scheduled for BEF, skipping submission...'
else:
    print 'No work today / error, skipping submission...'

# submit = browser.find_element_by_xpath('//input[@value="Submit"]')
# submit.click()
print >> text_file, 'FORECAST_DATA = ', FORECAST_DATA
print >> text_file2, 'FORECAST_SUMS = ', FORECAST_SUMS
# browser.close()
# browser.quit()
text_file.close()
text_file2.close()
