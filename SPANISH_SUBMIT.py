from codecs import open
from datetime import datetime, time, timedelta

from selenium import webdriver
from ESPANOL_1_DATA import ESPANOL_1_DATA
from ESPANOL_3_DATA import ESPANOL_3_DATA
from ESPANOL_NBC_DATA import ESPANOL_NBC_DATA

from schedule import SCHEDULE

TODAY = datetime.combine(datetime.today(), time.min)
YESTERDAY = (TODAY - datetime(1970,1,2)).total_seconds()
YDATE = datetime.today() - timedelta(days=1)
DATE_URL = YDATE.strftime("%Y/%-m/%-d")
TEMP_DATE_URL = TODAY.strftime("%Y/%-m/%-d")

print(DATE_URL)

print("Today's schedule: " + SCHEDULE[TODAY.strftime('%m.%d.%Y')])


DUMMY_DATA = {}

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
browser = webdriver.Chrome(options=options)

browser.get(
    'http://weatherate.com/forecast_bulk_entry/2/' + str(DATE_URL))

# Sign in to admin portal
unameBox = browser.find_element_by_name('username')
passBox = browser.find_element_by_name('password')
submitBox = browser.find_element_by_xpath("//button[@type='submit']")
unameBox.send_keys('connorg')
passBox.send_keys('TokyoGenso21')
submitBox.click()

# Get latest form names
stationNums = {}
for x in range(0, 57):
    ename = "form-" + str(x) + "-station_name"
    stationNameBox = browser.find_element_by_name(ename)
    stationNums[str(stationNameBox.get_attribute("value"))] = str(x)
    print(("Station " + str(x) + " is " + stationNameBox.get_attribute("value")))


# Enter data
for network in ESPANOL_1_DATA, ESPANOL_3_DATA, ESPANOL_NBC_DATA:
    for station in network:
        formNum = stationNums[station]
        print('Entering data for ' + station + ' / ' + str(formNum))
        elemPrefix = 'form-' + str(formNum) + '-'
        for forecast_type, forecast_value in network[station].items():
            if forecast_type.startswith('high'):
                if forecast_type.endswith('[0]'):
                    print('is d1_high')
                    d1hbox = browser.find_element_by_name(elemPrefix + 'd1_high')
                    if not d1hbox.get_attribute("value"):
                        d1hbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d1hbox.get_attribute("value"))
                elif forecast_type.endswith('[2]'):
                    print('is d2_high')
                    d2hbox = browser.find_element_by_name(elemPrefix + 'd2_high')
                    if not d2hbox.get_attribute("value"):
                        d2hbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d2hbox.get_attribute("value"))
                elif forecast_type.endswith('[3]'):
                    print('is d3_high')
                    d3hbox = browser.find_element_by_name(elemPrefix + 'd3_high')
                    if not d3hbox.get_attribute("value"):
                        d3hbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d3hbox.get_attribute("value"))
                elif forecast_type.endswith('[4]'):
                    print('is d4_high')
                    d4hbox = browser.find_element_by_name(elemPrefix + 'd4_high')
                    if not d4hbox.get_attribute("value"):
                        d4hbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d4hbox.get_attribute("value"))
                else:
                    print('What the fuck?')
            elif forecast_type.startswith('low'):
                if forecast_type.endswith('[1]'):
                    print('is d1n_low')
                    d1nlbox = browser.find_element_by_name(elemPrefix + 'd1n_low')
                    if not d1nlbox.get_attribute("value"):
                        d1nlbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d1nlbox.get_attribute("value"))
                elif forecast_type.endswith('[2]'):
                    print('is d2_low')
                    d2lbox = browser.find_element_by_name(elemPrefix + 'd2_low')
                    if not d2lbox.get_attribute("value"):
                        d2lbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d2lbox.get_attribute("value"))
                elif forecast_type.endswith('[3]'):
                    print('is d3_low')
                    d3lbox = browser.find_element_by_name(elemPrefix + 'd3_low')
                    if not d3lbox.get_attribute("value"):
                        d3lbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d3lbox.get_attribute("value"))
                elif forecast_type.endswith('[4]'):
                    print('is d4_low')
                    d4lbox = browser.find_element_by_name(elemPrefix + 'd4_low')
                    if not d4lbox.get_attribute("value"):
                        d4lbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d4lbox.get_attribute("value"))
                else:
                    print('What the fuck?')
            elif forecast_type.startswith('weather'):
                if forecast_type.endswith('[0]'):
                    print('is d1')
                    d1box = browser.find_element_by_name(elemPrefix + 'd1')
                    if not d1box.get_attribute("value"):
                        d1box.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d1box.get_attribute("value"))
                elif forecast_type.endswith('[1]'):
                    print('is d1n')
                    d1nbox = browser.find_element_by_name(elemPrefix + 'd1n')
                    if not d1nbox.get_attribute("value"):
                        d1nbox.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d1nbox.get_attribute("value"))
                elif forecast_type.endswith('[2]'):
                    print('is d2')
                    d2box = browser.find_element_by_name(elemPrefix + 'd2')
                    if not d2box.get_attribute("value"):
                        d2box.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d2box.get_attribute("value"))
                elif forecast_type.endswith('[3]'):
                    print('is d3')
                    d3box = browser.find_element_by_name(elemPrefix + 'd3')
                    if not d3box.get_attribute("value"):
                        d3box.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d3box.get_attribute("value"))
                elif forecast_type.endswith('[4]'):
                    print('is d4')
                    d4box = browser.find_element_by_name(elemPrefix + 'd4')
                    if not d4box.get_attribute("value"):
                        d4box.send_keys(forecast_value)
                    else:
                        print('Cell already containted text: ' + d4box.get_attribute("value"))
                else:
                    print('What the fuck?')
            else:
                print('what the fuck?')

if SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'BEF':
    print('Submitting...')
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'BEF OWDES':
    print('Submitting...')
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'OWDES BEF':
    print('Submitting...')
    submit = browser.find_element_by_xpath('//input[@value="Submit"]')
    submit.click()
elif SCHEDULE[TODAY.strftime('%m.%d.%Y')] == 'OWDES':
    print('Today is not scheduled for BEF, skipping submission...')
else:
    print('No work today / error, skipping submission...')

# browser.close()
# browser.quit()
