#!/bin/sh
printf 'Clearing output directory...\n'
rm -rf /home/gwynn/Work/Weatherate/output
mkdir /home/gwynn/Work/Weatherate/output
cp /home/gwynn/Work/Weatherate/INITIAL_SUBMIT.py /home/gwynn/Work/Weatherate/output/INITIAL_SUBMIT.py
cp /home/gwynn/Work/Weatherate/SPANISH_SUBMIT.py /home/gwynn/Work/Weatherate/output/SPANISH_SUBMIT.py
cp /home/gwynn/Work/Weatherate/SECONDARY_SUBMIT.py /home/gwynn/Work/Weatherate/output/SECONDARY_SUBMIT.py
cp /home/gwynn/Work/Weatherate/network_codes.py /home/gwynn/Work/Weatherate/output/network_codes.py
cp /home/gwynn/Work/Weatherate/weather_codes.py /home/gwynn/Work/Weatherate/output/weather_codes.py
cp /home/gwynn/Work/Weatherate/tests/test_dict.py /home/gwynn/Work/Weatherate/output/test_dict.py
cp /home/gwynn/Work/Weatherate/tests/test_codes.py /home/gwynn/Work/Weatherate/output/test_codes.py
cp /home/gwynn/Work/Weatherate/schedule.py /home/gwynn/Work/Weatherate/output/schedule.py
cp /home/gwynn/Work/Weatherate/city_codes.py /home/gwynn/Work/Weatherate/output/city_codes.py
CURRENTDATE=$(date +%Y-%m-%d)
echo ${CURRENTDATE}
echo ${CURRENTDATE} >> /home/gwynn/Work/Weatherate/output/currentdate.log
printf 'Establishing empty data files...\n'
echo "ABC_DATA={}" >> /home/gwynn/Work/Weatherate/output/ABC_DATA.py
printf 'Scraping Networks...\n'
/usr/bin/python2 /home/gwynn/Work/Weatherate/ABC.py
