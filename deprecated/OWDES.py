from codecs import open
import time
from datetime import datetime, timedelta
from selenium import webdriver

options = webdriver.ChromeOptions()
options.add_argument('headless')
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
browser = webdriver.Chrome(options=options)

text_file = open("OWDES_TEXTOUTPUT.py", "w")
data_file = open("OWDES_DATA.py", "w")
error_file = open("OWDES_errors.py", 'a')

print('SCRAPING NATIONAL WEATHER SERVICE =======================================')

current_date = datetime.now()
current_date = current_date - timedelta(days = 2)
end_date = current_date + timedelta(days = 1)
current_day = current_date.strftime("%d").lower()
end_day = end_date.strftime("%d").lower()
# current_day = '24'
print('Focus date is the: ', current_day)

# ABC_DATA = {}
# ERROR_DATA = {}

BASE_URL = 'https://www.wrh.noaa.gov/mesowest/getobext.php?wfo=psr&sid='
ENTRY_COUNT_FLAG = '&num='

TESTING_CODES = {
    'COS': 'KCOS'
}
STATION_CODES = {
    'NYC': 'KNYC',
    'LAX': 'KLAX',
    'ORD': 'KORD',
    'PHL': 'KPHL',
    'DFW': 'KDFW',
    'DCA': 'KDCA',
    'IAH': 'KIAH',
    'OAK': 'KOAK',
    'SFO': 'KSFO',
    'SJC': 'KSJC',
    'ATL': 'KATL',
    'BOS': 'KBOS',
    'PHX': 'KPHX',
    'SEA': 'KSEA',
    'SRQ': 'KSRQ',
    'TPA': 'KTPA',
    'DTW': 'KDTW',
    'MSP': 'KMSP',
    'MIA': 'KMIA',
    'DIA': 'KDEN',
    'MCO': 'KMCO',
    'CLE': 'KCLE',
    'SAC': 'KSAC',
    'STL': 'KSTL',
    'PDX': 'KPDX',
    'CLT': 'KCLT',
    'PIT': 'KPIT',
    'RDU': 'KRDU',
    'BWI': 'KBWI',
    'BNA': 'KBNA',
    'IND': 'KIND',
    'SEE': 'KSEE',
    'SAN': 'KSAN',
    'SLC': 'KSLC',
    'SAT': 'KSAT',
    'BDL': 'KBDL',
    'MCI': 'KMCI',
    'CMH': 'KCMH',
    'CVG': 'KCVG',
    'MKE': 'KMKE',
    'PBI': 'KPBI',
    'AVL': 'KAVL',
    'GSP': 'KGSP',
    'ATT': 'KATT',
    'LAS': 'KLAS',
    'OKC': 'KOKC',
    'JAX': 'KJAX',
    'GRR': 'KGRR',
    'BHM': 'KBHM',
    'MDT': 'KMDT',
    'ABQ': 'KABQ',
    'ORF': 'KORF',
    'GSO': 'KGSO',
    'SDF': 'KSDF',
    'MEM': 'KMEM',
    'NEW': 'KNEW',
    'PVD': 'KPVD',
    'BUF': 'KBUF',
    'FAT': 'KFAT',
    'RIC': 'KRIC',
    'FMY': 'KFMY',
    'AVP': 'KAVP',
    'LIT': 'KLIT',
    'MOB': 'KMOB',
    'PNS': 'KPNS',
    'ALB': 'KALB',
    'TYS': 'KTYS',
    'TUL': 'KTUL',
    'LEX': 'KLEX',
    'DAY': 'KDAY',
    'TUC': 'KTUS',
    'HNL': 'PHNL',
    'ICT': 'KICT',
    'DSM': 'KDSM',
    'GRB': 'KGRB',
    'LYH': 'KLYH',
    'ROA': 'KROA',
    'FNT': 'KFNT',
    'MBS': 'KMBS',
    'GEG': 'KGEG',
    'CRW': 'KCRW',
    'OMA': 'KOMA',
    'SGF': 'KSGF',
    'ROC': 'KROC',
    'CAE': 'KCAE',
    'TOL': 'KTOL',
    'PWM': 'KPWM',
    'HSV': 'KHSV',
    'MSN': 'KMSN',
    'CGI': 'KCGI',
    'PAH': 'KPAH',
    'MWA': 'KMWA',
    'SHV': 'KSHV',
    'HRL': 'KHRL',
    'SYR': 'KSYR',
    'ACT': 'KACT',
    'COS': 'KCOS',
    'CMI': 'KCMI',
    'DEC': 'KDEC',
    'SPI': 'KSPI',
    'CHA': 'KCHA',
    'SAV': 'KSAV',
    'ELP': 'KELP',
    'SJU': 'TJSJ',
    'CID': 'KCID',
    'ALO': 'KALO',
    'CHS': 'KCHS',
    'JAN': 'KJAN',
    'MYR': 'KMYR'
}

OWDES_DATA = {}

for network, site in STATION_CODES.items():
    try:
        browser.get(BASE_URL + site + ENTRY_COUNT_FLAG + '68')
        DOM = browser.page_source.encode('utf-8')
        print((network + ' DATA:'))
        # holder values
        ROW_COUNT = 0
        HOURLYS = {}
        HOURLYS_HIGH = 0
        HOURLYS_LOW = 999
        HIGHS = {}
        DAY_HIGH = 0
        LOWS = {}
        DAY_LOW = 999
        RAIN = {}
        RAIN_PERIODS = {'morning': 'false', 'afternoon': 'false', 'night': 'false', 'overnight': 'false'}
        SNOW_COUNT = 0
        FREEZING_COUNT = 0
        GUSTS = {}
        GUST_STREAK = 0
        LAST_GUST_TIME = '' 
        WINDY_DAY = 'false'
        WINDY_NIGHT = 'false'
        WIND_SPEEDS = {}
        CLOUDS = {}
        DAY_CLOUD_COUNT = 0
        NIGHT_CLOUD_COUNT = 0
        data_table = browser.find_element_by_id('timeseries')
        data_table_rows = data_table.find_elements_by_tag_name('tr')
        # TABLE HEADER SCRAPE
        HEADER_COLUMNS = data_table_rows[0].find_elements_by_tag_name('th')
        COLUMN_COUNT = len(HEADER_COLUMNS)
        WIND_COLUMN = 0
        WEATHER_COLUMN = 0
        VISIBILITY_COLUMN = 0
        RAIN_COLUMN = 0
        TEMP_COLUMN = 0
        HIGH_COLUMN = 0
        LOW_COLUMN = 0
        CLOUD_COLUMN = 0
        print('There are ', COLUMN_COUNT, ' columns!')
        col_cycle = 0
        for col in HEADER_COLUMNS:
            try:
                col_html = col.find_element_by_tag_name('div').get_attribute('innerHTML')
                print(col_html)
                if col_html.startswith("Wind<br>Speed"):
                    print('FOUND WIND COLUMN! IT IS ', col_cycle + 1)
                    WIND_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("1 Hour"):
                    print('FOUND RAIN COLUMN! IT IS ', col_cycle + 1)
                    RAIN_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("Weather"):
                    print('FOUND WEATHER COLUMN! IT IS ', col_cycle + 1)
                    WEATHER_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("Visibility"):
                    print('FOUND VISIBILITY COLUMN! IT IS ', col_cycle + 1)
                    VISIBILITY_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("Temp<br><br>(F)"):
                    print('FOUND TEMP COLUMN! IT IS ', col_cycle + 1)
                    TEMP_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("6 Hr<br>Max<br>(F)"):
                    print('FOUND HIGH COLUMN! IT IS ', col_cycle + 1)
                    HIGH_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("6 Hr<br>Min<br>(F)"):
                    print('FOUND LOW COLUMN! IT IS ', col_cycle + 1)
                    LOW_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                elif col_html.startswith("Clouds"):
                    print('FOUND CLOUD COLUMN! IT IS ', col_cycle + 1)
                    CLOUD_COLUMN = col_cycle
                    col_cycle = col_cycle + 1
                else: 
                    col_cycle = col_cycle + 1
            except:
                print('yikes')
                col_cycle = col_cycle + 1
        for row in data_table_rows:
            try:
                data_timestamp = row.find_element_by_tag_name('span').get_attribute('innerHTML')
                data_datetime = datetime.strptime(data_timestamp, "%d %b %I:%M %p")
                print(data_datetime)
                data_date = data_datetime.strftime("%d").lower()
                data_time = data_datetime.strftime("%I:%M %p").lower()
                print('Data date: ', data_date)
                print('Full date: ', data_timestamp)
                print('Data time: ', data_time)
                try:
                    if ((data_date == current_day) and (data_datetime.hour > 6)) or ((data_date == end_day) and (data_datetime.hour < 6)):
                        # Find high temps
                        try:
                            temp_high = row.find_elements_by_tag_name('td')[HIGH_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                            print('found high: ', temp_high)
                            HIGHS[data_datetime] = temp_high
                            if (data_datetime.hour >= 6) and (data_datetime.hour <= 24):
                                if int(temp_high) > int(DAY_HIGH):
                                    DAY_HIGH = temp_high
                                    print('New high is ', DAY_HIGH, '!!!!!!!!!!!!!!!!!!!!!')
                                else:
                                    print('...')
                            else:
                                print('...')
                        except:
                            print('could not find high for this row')
                        # Find low temps
                        try:
                            temp_low = row.find_elements_by_tag_name('td')[LOW_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                            print('found low: ', temp_low)
                            LOWS[data_datetime] = temp_low
                            if (data_date == current_day) and (data_datetime.hour >= 1) and (data_datetime.hour <= 7):
                                if int(temp_low) < int(DAY_LOW):
                                    DAY_LOW = temp_low
                                    print('New low is ', DAY_LOW, '!!!!!!!!!!!!1!')
                                else:
                                    print('...')
                            else:
                                print('...')
                        except:
                            print('could not find low for this row')
                        # Find precip
                        try:
                            rain_entry = row.find_elements_by_tag_name('td')[RAIN_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                            if rain_entry != 'T':
                                print('found precip: ', rain_entry, ' at ', data_time)
                                RAIN[data_datetime] = rain_entry
                                if (data_datetime.hour < 6):
                                    print('Precip was over night... skip.')

                                elif (data_datetime.hour >= 6) and (data_datetime.hour < 12):
                                    print('Precip in the morning!')
                                    try:
                                        weather_entry = row.find_elements_by_tag_name('td')[WEATHER_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                                        if "Snow" in weather_entry:
                                            print('found snow')
                                            try:
                                                visibility_entry = row.find_elements_by_tag_name('td')[VISIBILITY_COLUMN].find_element_by_tag_name('font').find_element_by_tag_name('b').get_attribute('innerHTML')
                                                print('YO FUCKKKKKKKKKKKKKKKKKKKKK ------------------------------------------------------------------------------------')
                                                print(visibility_entry)
                                                if int(float(visibility_entry)) < 5:
                                                    print('Not flurrues!')
                                                    SNOW_COUNT = SNOW_COUNT + 1
                                                    RAIN_PERIODS['morning'] = 'true'
                                                else:
                                                    print('flurries')
                                            except:
                                                print('Could not fetch visibility FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK')
                                        elif "Freezing" in weather_entry:
                                            print('found freezing rain')
                                            FREEZING_COUNT = FREEZING_COUNT + 1
                                            RAIN_PERIODS['morning'] = 'true'
                                        else:
                                            print('Did not find snow or ice, therefore rain')
                                            RAIN_PERIODS['morning'] = 'true'
                                    except:
                                        RAIN_PERIODS['morning'] = 'true'                                    
                    

                                elif (data_datetime.hour >= 12) and (data_datetime.hour < 18):
                                    print('Precip in the afternoon!')
                                    try:
                                        weather_entry = row.find_elements_by_tag_name('td')[WEATHER_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                                        if "Snow" in weather_entry:
                                            print('found snow')
                                            try:
                                                visibility_entry = row.find_elements_by_tag_name('td')[VISIBILITY_COLUMN].find_element_by_tag_name('font').find_element_by_tag_name('b').get_attribute('innerHTML')
                                                print('YO FUCKKKKKKKKKKKKKKKKKKKKK ------------------------------------------------------------------------------------')
                                                print(visibility_entry)
                                                if int(float(visibility_entry)) < 5:
                                                    print('Not flurrues!')
                                                    SNOW_COUNT = SNOW_COUNT + 1
                                                    RAIN_PERIODS['afternoon'] = 'true'
                                                else:
                                                    print('flurries')
                                            except:
                                                print('Could not fetch visibility FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK')
                                        elif "Freezing" in weather_entry:
                                            print('found freezing rain')
                                            FREEZING_COUNT = FREEZING_COUNT + 1
                                            RAIN_PERIODS['afternoon'] = 'true'
                                        else:
                                            print('Did not find snow or ice, therefore rain')
                                            RAIN_PERIODS['afternoon'] = 'true'
                                    except:
                                        RAIN_PERIODS['afternoon'] = 'true' 

                                elif (data_datetime.hour >= 18) and (data_datetime.hour < 22):
                                    print('Precip at night!')
                                    try:
                                        weather_entry = row.find_elements_by_tag_name('td')[WEATHER_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                                        if "Snow" in weather_entry:
                                            print('found snow')
                                            try:
                                                visibility_entry = row.find_elements_by_tag_name('td')[VISIBILITY_COLUMN].find_element_by_tag_name('font').find_element_by_tag_name('b').get_attribute('innerHTML')
                                                print('YO FUCKKKKKKKKKKKKKKKKKKKKK ------------------------------------------------------------------------------------')
                                                print(visibility_entry)
                                                if int(float(visibility_entry)) < 5:
                                                    print('Not flurrues!')
                                                    SNOW_COUNT = SNOW_COUNT + 1
                                                    RAIN_PERIODS['night'] = 'true'
                                                else:
                                                    print('flurries')
                                            except:
                                                print('Could not fetch visibility FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK')
                                        elif "Freezing" in weather_entry:
                                            print('found freezing rain')
                                            FREEZING_COUNT = FREEZING_COUNT + 1
                                            RAIN_PERIODS['night'] = 'true'
                                        else:
                                            print('Did not find snow or ice, therefore rain')
                                            RAIN_PERIODS['night'] = 'true'
                                    except:
                                        RAIN_PERIODS['night'] = 'true' 
                                
                                elif (data_datetime.hour >= 22):
                                    print('Precip was over night...')
                                    try:
                                        weather_entry = row.find_elements_by_tag_name('td')[WEATHER_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                                        if "Snow" in weather_entry:
                                            print('found snow')
                                            try:
                                                visibility_entry = row.find_elements_by_tag_name('td')[VISIBILITY_COLUMN].find_element_by_tag_name('font').find_element_by_tag_name('b').get_attribute('innerHTML')
                                                print('YO FUCKKKKKKKKKKKKKKKKKKKKK ------------------------------------------------------------------------------------')
                                                print(visibility_entry)
                                                if int(float(visibility_entry)) < 5:
                                                    print('Not flurrues!')
                                                    SNOW_COUNT = SNOW_COUNT + 1
                                                    RAIN_PERIODS['overnight'] = 'true'
                                                else:
                                                    print('flurries')
                                            except:
                                                print('Could not fetch visibility FUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUCK')
                                        elif "Freezing" in weather_entry:
                                            print('found freezing rain')
                                            FREEZING_COUNT = FREEZING_COUNT + 1
                                            RAIN_PERIODS['overnight'] = 'true'
                                        else:
                                            print('Did not find snow or ice, therefore rain')
                                            RAIN_PERIODS['overnight'] = 'true'
                                    except:
                                        RAIN_PERIODS['overnight'] = 'true'
                                else:
                                    print('shit what??')
                            else:
                                print('Just drizzle')
                        except:
                            print('could not find rain for this row')
                        # Find wind gusts
                        try:
                            gust_entry = row.find_elements_by_tag_name('td')[WIND_COLUMN].find_element_by_css_selector("[color='#FF00FF']").get_attribute('innerHTML')
                            print('found wind gust: ', gust_entry)
                            GUSTS[data_datetime] = gust_entry
                            if GUST_STREAK == 0:
                                LAST_GUST_TIME = data_datetime.hour
                                GUST_STREAK = GUST_STREAK + 1
                            else:
                                if (data_datetime.hour == LAST_GUST_TIME) or (data_datetime.hour == LAST_GUST_TIME - 1) or (data_datetime.hour == LAST_GUST_TIME + 1):
                                    LAST_GUST_TIME = data_datetime.hour
                                    GUST_STREAK = GUST_STREAK + 1
                                    if GUST_STREAK >= 5: 
                                        if (LAST_GUST_TIME < 6):
                                            WINDY_NIGHT = 'true'
                                        elif (LAST_GUST_TIME >= 6) and (LAST_GUST_TIME <= 22):
                                            WINDY_DAY = 'true'
                                        elif (LAST_GUST_TIME > 22):
                                            WINDY_NIGHT = 'true'
                                        else:
                                            print('wind error')
                                else:
                                    print('broken streak')
                        except:
                            print('could not find wind gusts for this row')
                        # Find average wind speed
                        # try:
                            # wind_entry = row.find_elements_by_tag_name('td')[5].find_element_by_tag_name('font').get_attribute('innerHTML')
                            # print 'wind speed is: ', wind_entry
                            # if wind_entry != 'CALM':
                                # WIND_SPEEDS[data_timestamp] = int(wind_entry)
                            # else:
                                # print 'wind is calm'
                        # except:
                            # print 'could not find wind speed for this row'
                        # Find clouds
                        try:
                            cloud_entry = row.find_elements_by_tag_name('td')[CLOUD_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                            print('found clouds: ', cloud_entry)
                            if (cloud_entry.startswith( 'BKN' )) or (cloud_entry.startswith( 'OVC' )):
                                if int(cloud_entry[3:]) <= 200:
                                    CLOUDS[data_datetime] = cloud_entry
                                    if (data_datetime.hour < 6):
                                        NIGHT_CLOUD_COUNT = NIGHT_CLOUD_COUNT + 1

                                    elif (data_datetime.hour >= 6) and (data_datetime.hour < 22):
                                        DAY_CLOUD_COUNT = DAY_CLOUD_COUNT + 1

                                    elif (data_datetime.hour >= 22):
                                        NIGHT_CLOUD_COUNT = NIGHT_CLOUD_COUNT + 1
                                    else:
                                        print('...')
                                else:
                                    print('...')
                            else:
                                print('clouds are minor')
                        except:
                            print('could not find clouds')
                    elif ((data_date == current_day) and (data_datetime.hour <= 6)):
                        print('row within range')
                        ROW_COUNT = ROW_COUNT + 1
                        print('UPDATED ROW COUNT: ', ROW_COUNT)
                        # Find hourly temp
                        try:
                            temp_hourly = row.find_elements_by_tag_name('td')[TEMP_COLUMN].find_element_by_tag_name('font').get_attribute('innerHTML')
                            print('found hourly temp: ', temp_hourly)
                            HOURLYS[data_datetime] = temp_hourly
                            # modify for low rules
                            if (data_datetime.hour >= 1) and (data_datetime.hour <= 7):
                                if int(temp_hourly) < int(HOURLYS_LOW):
                                    HOURLYS_LOW = temp_hourly
                                    print('New hourly low is ', HOURLYS_LOW, '!!!!!!!!!!!!')
                                else:
                                    print('...')
                            else:
                                print('...')
                        except:
                            print('could not find hourly for this row')
                    else:
                        print('row does not compute')            
                except:
                    print('something went wrong')
            except:
                print('could not find date')
        # print 'FINAL HIGHS: ', HIGHS
        # print 'FINAL HIGHS SORTED: ', sorted(HIGHS.items())
        # print 'FINAL LOWS: ', LOWS
        # print 'FINAL RAIN: ', RAIN
        # print 'FINAL WIND SPEEDS: ', WIND_SPEEDS
        # WIND_SPEED_COUNT = 0
        # WIND_SPEED_SUM = 0
        # for time, speed in WIND_SPEEDS.iteritems():
            # WIND_SPEED_COUNT = WIND_SPEED_COUNT + 1
            # WIND_SPEED_SUM = WIND_SPEED_SUM + speed
        # AVERAGE_WIND_SPEED = WIND_SPEED_SUM / WIND_SPEED_COUNT
        # print 'AVERAGE WIND SPEED: ', AVERAGE_WIND_SPEED 
        # print 'FINAL CLOUDS: ', CLOUDS
        # print 'OVC / BKN DAY COUNT: ', DAY_CLOUD_COUNT
        # print 'OVC / BKN NIGHT COUNT: ', NIGHT_CLOUD_COUNT
        # PRINT RESULTS
        OWDES_DATA[network] = {
            'hourly[' + network + ']': '',
            'high['+ network + ']': '',
            'low['+ network + ']': '',
            'combinedLow['+ network + ']': '',
            'sky['+ network + ']': '',
            'sky_n['+ network + ']': '',
            'tstorm['+ network + ']': '',
            'wind['+ network + ']': '',
            'fog['+ network + ']': '',
            'tstorm_n['+ network + ']': '',
            'wind_n['+ network + ']': '',
            'fog_n['+ network + ']': '',
            'morning['+ network + ']': '',
            'afternoon['+ network + ']': '',
            'evening['+ network + ']': '',
        }
        print('RESULTS =========================================')
        print('CITY: ', network, '=================', file=text_file)
        print('FINAL HIGH: ', DAY_HIGH) 
        OWDES_DATA[network]['high['+ network + ']'] = str(DAY_HIGH)
        print('HIGH: ', DAY_HIGH, file=text_file)
        print('FINAL HOURLY LOW: ', HOURLYS_LOW)
        OWDES_DATA[network]['hourly[' +  network + ']'] = str(HOURLYS_LOW)
        print('HOURLYS: ', HOURLYS_LOW, file=text_file)
        print('FINAL LOW: ', DAY_LOW) 
        OWDES_DATA[network]['low['+ network + ']'] = str(DAY_LOW)
        print('LOW: ', DAY_LOW, file=text_file)
        try:
            if (int(DAY_LOW) < int(HOURLYS_LOW)):
                print('6-hour was lower. Final low is: ', DAY_LOW)
                OWDES_DATA[network]['combinedLow['+ network + ']'] = str(DAY_LOW)
            else:
                print('6-hour was not lower. Final low is: ', HOURLYS_LOW)
                OWDES_DATA[network]['combinedLow['+ network + ']'] = str(HOURLYS_LOW)
        except:
            print('Wut teh fuuuuck')
            
        print('SNOW COUNT: ', SNOW_COUNT)
        print('FREEZING COUNT: ', FREEZING_COUNT)
        print('SNOW COUNT: ', SNOW_COUNT, file=text_file)
        print('FREEZING COUNT: ', FREEZING_COUNT, file=text_file)
        # Print day status
        if (RAIN_PERIODS['morning'] == 'true') or (RAIN_PERIODS['afternoon'] == 'true'):
            if SNOW_COUNT != 0 and FREEZING_COUNT != 0:
                print('DAY IS MIXED')
                OWDES_DATA[network]['sky['+ network + ']'] = 'mixed'
                print('DAY: ', 'mixed', file=text_file)
            elif SNOW_COUNT != 0 and FREEZING_COUNT == 0:
                print('DAY IS SNOWY')
                OWDES_DATA[network]['sky['+ network + ']'] = 'snow'
                print('DAY: ', 'snow', file=text_file)
            elif SNOW_COUNT == 0 and FREEZING_COUNT != 0:
                print('DAY IS ICEY')
                OWDES_DATA[network]['sky['+ network + ']'] = 'ice'
                print('DAY: ', 'ice', file=text_file)
            else:
                print('DAY IS RAINY')
                OWDES_DATA[network]['sky['+ network + ']'] = 'rainy'
                print('DAY: ', 'rainy', file=text_file)
        elif DAY_CLOUD_COUNT >= 7:
            print('DAY IS CLOUDY')
            OWDES_DATA[network]['sky['+ network + ']'] = 'cloudy'
            print('DAY: ', 'cloudy', file=text_file)
        else:
            print('DAY IS SUNNY')
            OWDES_DATA[network]['sky['+ network + ']'] = 'sunny'
            print('DAY: ', 'sunny', file=text_file)
        # Print night status
        if (RAIN_PERIODS['overnight'] == 'true'):
            if SNOW_COUNT != 0 and FREEZING_COUNT != 0:
                print('NIGHT IS MIXED')
                OWDES_DATA[network]['sky_n['+ network + ']'] = 'mixed'
                print('NIGHT: ', 'mixed', file=text_file)
            elif SNOW_COUNT != 0 and FREEZING_COUNT == 0:
                print('NIGHT IS SNOWY')
                OWDES_DATA[network]['sky_n['+ network + ']'] = 'snow'
                print('NIGHT: ', 'snow', file=text_file)
            elif SNOW_COUNT == 0 and FREEZING_COUNT != 0:
                print('NIGHT IS ICEY')
                OWDES_DATA[network]['sky_n['+ network + ']'] = 'ice'
                print('NIGHT: ', 'ice', file=text_file)
            else:
                print('NIGHT IS RAINY')
                OWDES_DATA[network]['sky_n['+ network + ']'] = 'rainy'
                print('NIGHT: ', 'rainy', file=text_file)
        elif NIGHT_CLOUD_COUNT >= 3:
            print('NIGHT IS CLOUDY')
            OWDES_DATA[network]['sky_n['+ network + ']'] = 'cloudy'
            print('NIGHT: ', 'cloudy', file=text_file)
        else:
            print('NIGHT IS CLEAR')
            OWDES_DATA[network]['sky_n['+ network + ']'] = 'clear'
            print('NIGHT: ', 'clear', file=text_file)
        # print wind
        # print 'FINAL GUSTS: ', sorted(GUSTS.items())
        # print 'GUST STREAK: ', GUST_STREAK
        if WINDY_DAY == 'true':
            print('Windy day')
            OWDES_DATA[network]['wind['+ network + ']'] = 'true'
            print('WINDY DAY: ', 'true', file=text_file)

        if WINDY_NIGHT == 'true':
            print('Windy night')
            OWDES_DATA[network]['wind_n['+ network + ']'] = 'true'
            print('WINDY NIGHT: ', 'true', file=text_file)

        # print rain times
        print('PRECIP: ', RAIN_PERIODS)
        OWDES_DATA[network]['morning['+ network + ']'] = RAIN_PERIODS['morning']
        OWDES_DATA[network]['afternoon['+ network + ']'] = RAIN_PERIODS['afternoon']
        OWDES_DATA[network]['evening['+ network + ']'] = RAIN_PERIODS['night']
        print('RAIN PERIODS: ', 'morning ', RAIN_PERIODS['morning'], ' afternoon ', RAIN_PERIODS['afternoon'], ' night ', RAIN_PERIODS['night'], file=text_file)

    except:
        print('Could not fetch site ' + network)
        # ERROR_DATA[network] = {'error': 'could not fetch'}
        
print('OWDES_DATA = ', OWDES_DATA, file=data_file)
# print >> error_file, 'ABC_ERRORS = ', ERROR_DATA
# browser.close()
# browser.quit()
# text_file.close()
