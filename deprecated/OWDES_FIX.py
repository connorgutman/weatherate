from codecs import open
from datetime import datetime, time, timedelta
from selenium import webdriver

DUMMY_DATA = {}

TODAY = datetime.combine(datetime.today(), time.min)
YESTERDAY = (TODAY - datetime(1970,1,2)).total_seconds()
YDATE = datetime.today() - timedelta(days=2)
DATE_URL = YDATE.strftime("%Y/%-m/%-d")
TEMP_DATE_URL = TODAY.strftime("%Y/%-m/%-d")

print DATE_URL

browser = webdriver.Chrome(
    executable_path='/usr/bin/chromedriver')

browser.get(
    'http://weatherate.com/observation_entry/2020/3/10')

# Sign in to admin portal
unameBox = browser.find_element_by_name('username')
passBox = browser.find_element_by_name('password')
submitBox = browser.find_element_by_xpath("//button[@type='submit']")
unameBox.send_keys('connorg')
passBox.send_keys('TokyoGenso21')
submitBox.click()

WRONGstationRows = {
    "NYC": "0",
    "LAX": "1",
    "ORD": "2",
    "PHL": "3",
    "DFW": "4",
    "DCA": "5",
    "IAH": "6",
    "OAK": "7",
    "SFO": "8",
    "SJC": "9",
    "ATL": "10",
    "BOS": "11",
    "PHX": "12",
    "SEA": "13",
    "SRQ": "14",
    "TPA": "15",
    "DTW": "16",
    "MSP": "17",
    "MIA": "18",
    "DIA": "19",
    "MCO": "20",
    "CLE": "21",
    "SAC": "22",
    "STL": "23",
    "PDX": "24",
    "CLT": "25",
    "PIT": "26",
    "RDU": "27",
    "BWI": "28",
    "BNA": "29",
    "IND": "30",
    "SEE": "31",
    "SAN": "32",
    "SLC": "33",
    "SAT": "34",
    "BDL": "35",
    "MCI": "36",
    "CMH": "37",
    "CVG": "38",
    "MKE": "39",
    "PBI": "40",
    "AVL": "41",
    "GSP": "42",
    "ATT": "43",
    "LAS": "44",
    "OKC": "45",
    "JAX": "46",
    "GRR": "47",
    "BHM": "48",
    "MDT": "49",
    "ABQ": "50",
    "ORF": "51",
    "GSO": "52",
    "SDF": "53",
    "MEM": "54",
    "NEW": "55",
    "PVD": "56",
    "BUF": "57",
    "FAT": "58",
    "RIC": "59",
    "FMY": "60",
    "AVP": "61",
    "LIT": "62",
    "MOB": "63",
    "PNS": "64",
    "ALB": "65",
    "TYS": "66",
    "TUL": "67",
    "LEX": "68",
    "DAY": "69",
    "TUC": "70",
    "HNL": "71",
    "ICT": "72",
    "DSM": "73",
    "GRB": "74",
    "LYH": "75",
    "ROA": "76",
    "FNT": "77",
    "MBS": "78",
    "GEG": "79",
    "CRW": "80",
    "OMA": "81",
    "SGF": "82",
    "ROC": "83",
    "CAE": "84",
    "TOL": "85",
    "PWM": "86",
    "HSV": "87",
    "MSN": "88",
    "CGI": "89",
    "PAH": "90",
    "MWA": "91",
    "SHV": "92",
    "HRL": "93",
    "SYR": "94",
    "ACT": "95",
    "COS": "96",
    "CMI": "97",
    "DEC": "98",
    "SPI": "99"
}

stationRows = {
        "NYC": "0",
        "LAX": "1",
        "ORD": "2",
        "PHL": "3",
        "DFW": "4",
        "OAK": "5",
        "SFO": "6",
        "SJC": "7",
        "DCA": "8",
        "IAH": "9",
        "BOS": "10",
        "ATL": "11",
        "PHX": "12",
        "SRQ": "13",
        "TPA": "14",
        "SEA": "15",
        "DTW": "16",
        "MSP": "17",
        "MIA": "18",
        "DIA": "19",
        "MCO": "20",
        "CLE": "21",
        "SAC": "22",
        "CLT": "23",
        "PDX": "24",
        "STL": "25",
        "PIT": "26",
        "IND": "27",
        "BWI": "28",
        "RDU": "29",
        "BNA": "30",
        "SEE": "31",
        "SAN": "32",
        "SLC": "33",
        "SAT": "34",
        "MCI": "35",
        "BDL": "36",
        "CMH": "37",
        "MKE": "38",
        "PBI": "39",
        "CVG": "40",
        "AVL": "41",
        "GSP": "42",
        "LAS": "43",
        "ATT": "44",
        "JAX": "45",
        "ORF": "46",
        "OKC": "47",
        "BHM": "48",
        "GRR": "49",
        "ABQ": "50",
        "MDT": "51",
        "SDF": "52",
        "GSO": "53",
        "NEW": "54",
        "MEM": "55",
        "BUF": "56",
        "FMY": "57",
        "RIC": "58",
        "FAT": "59",
        "PVD": "60",
        "MOB": "61",
        "PNS": "62",
        "TUL": "63",
        "ALB": "64",
        "AVP": "65",
        "TYS": "66",
        "LIT": "67",
        "DAY": "68",
        "LEX": "69",
        "TUC": "70",
        "HNL": "71",
        "GRB": "72",
        "DSM": "73",
        "LYH": "74",
        "ROA": "75",
        "GEG": "76",
        "OMA": "77",
        "ICT": "78",
        "SGF": "79",
        "CRW": "80",
        "CAE": "81",
        "ROC": "82",
        "FNT": "83",
        "MBS": "84",
        "HSV": "85",
        "PWM": "86",
        "TOL": "87",
        "MSN": "88",
        "ACT": "89",
        "HRL": "90",
        "CGI": "91",
        "PAH": "92",
        "MWA": "93",
        "COS": "94",
        "SHV": "95",
        "SYR": "96",
        "CMI": "97",
        "DEC": "98",
        "SPI": "99"
}

WRONG_DATA =  {}


for station, code in WRONGstationRows.iteritems():
    print 'Checking ' + station + ' / ' + code
    box1 = browser.find_element_by_name('form-' + code + '-high')
    box2 = browser.find_element_by_name('form-' + code + '-low')

    high = box1.get_property("value")
    low = box2.get_property("value")

    box3A = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '1' + "']")
    box3B = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '2' + "']")
    box3C = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '3' + "']")
    box3D = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '4' + "']")
    box3E = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '5' + "']")
    box3F = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '6' + "']")

    if box3A.is_selected():
        print 'sun selected'
        box3 = box3A
        precip = 'sunny'
    elif box3B.is_selected():
        print 'cloud selected'
        box3 = box3B
        precip = 'cloudy'
    elif box3C.is_selected():
        print 'rain selected'
        box3 = box3C
        precip = 'rain'
    elif box3D.is_selected():
        print 'snow selected'
        box3 = box3D
        precip = 'snow'
    elif box3E.is_selected():
        print 'ice selected'
        box3 = box3E
        precip = 'ice'
    elif box3F.is_selected():
        print 'mixed selected'
        box3 = box3F
        precip = 'mixed'
    else:
        print 'could not figure out what is selected.'

    box4 = browser.find_element_by_name('form-' + code + '-windy')
    box5 = browser.find_element_by_name('form-' + code + '-windy_n')
    box6 = browser.find_element_by_name('form-' + code + '-morning')
    box7 = browser.find_element_by_name('form-' + code + '-afternoon')
    box8 = browser.find_element_by_name('form-' + code + '-evening')

    if box4.is_selected():
        windyDay = 'true'
    else:
        windyDay = 'false'

    if box5.is_selected():
        windyNight = 'true'
    else:
        windyNight = 'false'
    
    if box6.is_selected():
        rainyMorning = 'true'
    else:
        rainyMorning = 'false'

    if box7.is_selected():
        rainyAfternoon = 'true'
    else:
        rainyAfternoon = 'false'

    if box8.is_selected():
        rainyEvening = 'true'
    else:
        rainyEvening = 'false'

    box9A = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '1' + "']")
    box9B = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '2' + "']")
    box9C = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '3' + "']")
    box9D = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '4' + "']")
    box9E = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '5' + "']")
    box9F = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '6' + "']")

    if box9A.is_selected():
        print 'sun selected'
        box9 = box9A
        precipN = 'sunny'
    elif box9B.is_selected():
        print 'cloud selected'
        box9 = box9B
        precipN = 'cloudy'
    elif box9C.is_selected():
        print 'rain selected'
        box9 = box9C
        precipN = 'rain'
    elif box9D.is_selected():
        print 'snow selected'
        box9 = box9D
        precipN = 'snow'
    elif box9E.is_selected():
        print 'ice selected'
        box9 = box9E
        precipN = 'ice'
    elif box9F.is_selected():
        print 'mixed selected'
        box9 = box9F
        precipN = 'mixed'
    else:
        print 'could not figure out what is selected.'

    print 'high: ' + high + ' low: ' + low + ' precip: ' + precip + ' precip_n: ' + precipN + ' windyDay: ' + windyDay + ' windyNight: ' + windyNight + ' rainyMorning: ' + rainyMorning + ' rainyAfternoon: ' + rainyAfternoon + ' rainyEvening: ' + rainyEvening 
    WRONG_DATA[station] = {
        'high': high,
        'low': low,
        'precip': precip,
        'precip_n': precipN,
        'windyDay': windyDay,
        'windyNight': windyNight,
        'rainyMorning': rainyMorning,
        'rainyAfternoon': rainyAfternoon,
        'rainyEvening': rainyEvening
    }


for station, code in stationRows.iteritems():
    print 'Updating ' + station + ' / ' + code
    box1 = browser.find_element_by_name('form-' + code + '-high')
    box2 = browser.find_element_by_name('form-' + code + '-low')
    box1.clear()
    box2.clear()
    box1.send_keys(WRONG_DATA[station]['high'])
    box2.send_keys(WRONG_DATA[station]['low'])

    box3A = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '1' + "']")
    box3B = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '2' + "']")
    box3C = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '3' + "']")
    box3D = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '4' + "']")
    box3E = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '5' + "']")
    box3F = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip' and @value='" + '6' + "']")

    if WRONG_DATA[station]['precip'] == 'sunny':
        print 'sun selected'
        box3A.click()
    elif WRONG_DATA[station]['precip'] == 'cloudy':
        print 'cloud selected'
        box3B.click()
    elif WRONG_DATA[station]['precip'] == 'rain':
        print 'rain selected'
        box3C.click()
    elif WRONG_DATA[station]['precip'] == 'snow':
        print 'snow selected'
        box3D.click()
    elif WRONG_DATA[station]['precip'] == 'ice':
        print 'ice selected'
        box3E.click()
    elif WRONG_DATA[station]['precip'] == 'mixed':
        print 'mixed selected'
        box3F.click()
    else:
        print 'could not figure out what is selected.'

    box4 = browser.find_element_by_name('form-' + code + '-windy')
    box5 = browser.find_element_by_name('form-' + code + '-windy_n')
    box6 = browser.find_element_by_name('form-' + code + '-morning')
    box7 = browser.find_element_by_name('form-' + code + '-afternoon')
    box8 = browser.find_element_by_name('form-' + code + '-evening')

    if WRONG_DATA[station]['windyDay'] == 'true':
        box4.click()
    else:
        print '.'

    if WRONG_DATA[station]['windyNight'] == 'true':
        box5.click()
    else:
        print '.'
    
    if WRONG_DATA[station]['rainyMorning'] == 'true':
        box6.click()
    else:
        print '.'

    if WRONG_DATA[station]['rainyAfternoon'] == 'true':
        box7.click()
    else:
        print '.'

    if WRONG_DATA[station]['rainyEvening'] == 'true':
        box8.click()
    else:
        print '.'

    box9A = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '1' + "']")
    box9B = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '2' + "']")
    box9C = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '3' + "']")
    box9D = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '4' + "']")
    box9E = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '5' + "']")
    box9F = browser.find_element_by_xpath("//input[@name='form-" + code + "-skyprecip_n' and @value='" + '6' + "']")

    if WRONG_DATA[station]['precip_n'] == 'sunny':
        print 'sun selected'
        box9A.click()
    elif WRONG_DATA[station]['precip_n'] == 'cloudy':
        print 'cloud selected'
        box9B.click()
    elif WRONG_DATA[station]['precip_n'] == 'rain':
        print 'rain selected'
        box9C.click()
    elif WRONG_DATA[station]['precip_n'] == 'snow':
        print 'snow selected'
        box9D.click()
    elif WRONG_DATA[station]['precip_n'] == 'ice':
        print 'ice selected'
        box9E.click()
    elif WRONG_DATA[station]['precip_n'] == 'mixed':
        print 'mixed selected'
        box9F.click()
    else:
        print 'could not figure out what is selected.'

# browser.close()
# browser.quit()
# text_file.close()
