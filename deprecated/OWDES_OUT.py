from codecs import open
from datetime import datetime, time, timedelta
from selenium import webdriver
from OWDES_DATA import OWDES_DATA

DUMMY_DATA = {}

TODAY = datetime.combine(datetime.today(), time.min)
YESTERDAY = (TODAY - datetime(1970,1,2)).total_seconds()
YDATE = datetime.today() - timedelta(days=2)
DATE_URL = YDATE.strftime("%Y/%-m/%-d")
TEMP_DATE_URL = TODAY.strftime("%Y/%-m/%-d")

print DATE_URL
options = webdriver.ChromeOptions()
options.add_argument('--no-sandbox')
options.add_argument('--disable-dev-shm-usage')
browser = webdriver.Chrome(options=options)

browser.get(
    'http://weatherate.com/observation_entry/' + str(DATE_URL))

# Sign in to admin portal
unameBox = browser.find_element_by_name('username')
passBox = browser.find_element_by_name('password')
submitBox = browser.find_element_by_xpath("//button[@type='submit']")
unameBox.send_keys('connorg')
passBox.send_keys('TokyoGenso21')
submitBox.click()

WRONGstationRows = {
    "NYC": "0",
    "LAX": "1",
    "ORD": "2",
    "PHL": "3",
    "DFW": "4",
    "DCA": "5",
    "IAH": "6",
    "OAK": "7",
    "SFO": "8",
    "SJC": "9",
    "ATL": "10",
    "BOS": "11",
    "PHX": "12",
    "SEA": "13",
    "SRQ": "14",
    "TPA": "15",
    "DTW": "16",
    "MSP": "17",
    "MIA": "18",
    "DIA": "19",
    "MCO": "20",
    "CLE": "21",
    "SAC": "22",
    "STL": "23",
    "PDX": "24",
    "CLT": "25",
    "PIT": "26",
    "RDU": "27",
    "BWI": "28",
    "BNA": "29",
    "IND": "30",
    "SEE": "31",
    "SAN": "32",
    "SLC": "33",
    "SAT": "34",
    "BDL": "35",
    "MCI": "36",
    "CMH": "37",
    "CVG": "38",
    "MKE": "39",
    "PBI": "40",
    "AVL": "41",
    "GSP": "42",
    "ATT": "43",
    "LAS": "44",
    "OKC": "45",
    "JAX": "46",
    "GRR": "47",
    "BHM": "48",
    "MDT": "49",
    "ABQ": "50",
    "ORF": "51",
    "GSO": "52",
    "SDF": "53",
    "MEM": "54",
    "NEW": "55",
    "PVD": "56",
    "BUF": "57",
    "FAT": "58",
    "RIC": "59",
    "FMY": "60",
    "AVP": "61",
    "LIT": "62",
    "MOB": "63",
    "PNS": "64",
    "ALB": "65",
    "TYS": "66",
    "TUL": "67",
    "LEX": "68",
    "DAY": "69",
    "TUC": "70",
    "HNL": "71",
    "ICT": "72",
    "DSM": "73",
    "GRB": "74",
    "LYH": "75",
    "ROA": "76",
    "FNT": "77",
    "MBS": "78",
    "GEG": "79",
    "CRW": "80",
    "OMA": "81",
    "SGF": "82",
    "ROC": "83",
    "CAE": "84",
    "TOL": "85",
    "PWM": "86",
    "HSV": "87",
    "MSN": "88",
    "CGI": "89",
    "PAH": "90",
    "MWA": "91",
    "SHV": "92",
    "HRL": "93",
    "SYR": "94",
    "ACT": "95",
    "COS": "96",
    "CMI": "97",
    "DEC": "98",
    "SPI": "99",
    "CHA": "100",
    "SAV": "101",
    "ELP": "102",
    "SJU": "103",
    "MYR": "104"
}

stationRows = {
        "NYC": "0",
        "LAX": "1",
        "ORD": "2",
        "PHL": "3",
        "DFW": "4",
        "OAK": "5",
        "SFO": "6",
        "SJC": "7",
        "DCA": "8",
        "IAH": "9",
        "BOS": "10",
        "ATL": "11",
        "PHX": "12",
        "SRQ": "13",
        "TPA": "14",
        "SEA": "15",
        "DTW": "16",
        "MSP": "17",
        "MIA": "18",
        "DIA": "19",
        "MCO": "20",
        "CLE": "21",
        "SAC": "22",
        "CLT": "23",
        "PDX": "24",
        "STL": "25",
        "PIT": "26",
        "IND": "27",
        "BWI": "28",
        "RDU": "29",
        "BNA": "30",
        "SEE": "31",
        "SAN": "32",
        "SLC": "33",
        "SAT": "34",
        "MCI": "35",
        "BDL": "36",
        "CMH": "37",
        "MKE": "38",
        "PBI": "39",
        "CVG": "40",
        "AVL": "41",
        "GSP": "42",
        "LAS": "43",
        "ATT": "44",
        "JAX": "45",
        "ORF": "46",
        "OKC": "47",
        "BHM": "48",
        "GRR": "49",
        "ABQ": "50",
        "MDT": "51",
        "SDF": "52",
        "GSO": "53",
        "NEW": "54",
        "MEM": "55",
        "BUF": "56",
        "FMY": "57",
        "RIC": "58",
        "FAT": "59",
        "PVD": "60",
        "MOB": "61",
        "PNS": "62",
        "TUL": "63",
        "ALB": "64",
        "AVP": "65",
        "TYS": "66",
        "LIT": "67",
        "DAY": "68",
        "LEX": "69",
        "TUC": "70",
        "HNL": "71",
        "GRB": "72",
        "DSM": "73",
        "LYH": "74",
        "ROA": "75",
        "GEG": "76",
        "OMA": "77",
        "ICT": "78",
        "SGF": "79",
        "CRW": "80",
        "CAE": "81",
        "ROC": "82",
        "FNT": "83",
        "MBS": "84",
        "HSV": "85",
        "PWM": "86",
        "TOL": "87",
        "MSN": "88",
        "ACT": "89",
        "HRL": "90",
        "CGI": "91",
        "PAH": "92",
        "MWA": "93",
        "COS": "94",
        "SHV": "95",
        "SYR": "96",
        "CMI": "97",
        "DEC": "98",
        "SPI": "99",
        "SAV": "100",
        "CID": "101",
        "ALO": "102",
        "CHS": "103",
        "CHA": "104",
        "ELP": "105",
        "JAN": "106",
        "MYR": "107",
        "SJU": "108"
}


for station in OWDES_DATA:
    print 'Entering data for ' + station
    for forecast_type, forecast_value in OWDES_DATA[station].iteritems():
        # print forecast_type
        # print forecast_value
        
        cityCode = forecast_type.split('[')[1].split(']')[0]

        if forecast_type.startswith('high'): 
            box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-high')
            if not box.get_attribute("value"):
                box.send_keys(forecast_value)
            else:
                print 'Cell already containted text: ' + box.get_attribute("value")

        elif forecast_type.startswith('combinedLow'): 
            box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-low')
            if not box.get_attribute("value"):
                box.send_keys(forecast_value)
            else:
                print 'Cell already containted text: ' + box.get_attribute("value")

        elif forecast_type.startswith('sky['):
            if (forecast_value == 'sunny') or (forecast_value == 'clear'):
                print 'Sky is sunny/clear'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '1' + "']")
                box.click()
            elif (forecast_value == 'cloudy'):
                print 'Sky is cloudy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '2' + "']")
                box.click()
            elif (forecast_value == 'rainy'):
                print 'Sky is rainy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '3' + "']")
                box.click()
            elif (forecast_value == 'snow'):
                print 'Sky is snowy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '4' + "']")
                box.click()
            elif (forecast_value == 'ice'):
                print 'Sky is icey'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '5' + "']")
                box.click()
            elif (forecast_value == 'mixed'):
                print 'Sky is mixed'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip' and @value='" + '6' + "']")
                box.click()
            else:
                print 'Sky is other'

        elif forecast_type.startswith('sky_n'):
            if (forecast_value == 'sunny') or (forecast_value == 'clear'):
                print 'Sky is sunny/clear'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '1' + "']")
                box.click()
            elif (forecast_value == 'cloudy'):
                print 'Sky is cloudy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '2' + "']")
                box.click()
            elif (forecast_value == 'rainy'):
                print 'Sky is rainy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '3' + "']")
                box.click()
            elif (forecast_value == 'snow'):
                print 'Sky is snowy'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '4' + "']")
                box.click()
            elif (forecast_value == 'ice'):
                print 'Sky is icey'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '5' + "']")
                box.click()
            elif (forecast_value == 'mixed'):
                print 'Sky is mixed'
                box = browser.find_element_by_xpath("//input[@name='form-" + stationRows[str(cityCode)] + "-skyprecip_n' and @value='" + '6' + "']")
                box.click()
            else:
                print 'Sky is other'

        elif forecast_type.startswith('wind['):
            if (forecast_value == 'true'):
                print 'Day/Night is windy'
                box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-windy')
                box.click()
            else:
                print 'Not windy'

        elif forecast_type.startswith('wind_n'):
            if (forecast_value == 'true'):
                print 'Day/Night is windy'
                box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-windy_n')
                box.click()
            else:
                print 'Not windy'

        elif forecast_type.startswith('morning'):
            if (forecast_value == 'true'):
                print 'Entering rain period'
                box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-morning')
                box.click()
            else:
                print 'Skipping rain period'

        elif forecast_type.startswith('afternoon'):
            if (forecast_value == 'true'):
                print 'Entering rain period'
                box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-afternoon')
                box.click()
            else:
                print 'Skipping rain period'
        
        elif forecast_type.startswith('evening'):
            if (forecast_value == 'true'):
                print 'Entering rain period'
                box = browser.find_element_by_name('form-' + stationRows[str(cityCode)] + '-evening')
                box.click()
            else:
                print 'Skipping rain period'

        else:
            print 'Skipping storm/fog/other'

# browser.close()
# browser.quit()
# text_file.close()
