CITY:  NYC =================
HIGH:  88
HOURLYS:  80
LOW:  79
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  LAX =================
HIGH:  75
HOURLYS:  65
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ORD =================
HIGH:  80
HOURLYS:  63
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PHL =================
HIGH:  90
HOURLYS:  75
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  DFW =================
HIGH:  91
HOURLYS:  81
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
WINDY DAY:  true
RAIN PERIODS:  morning  true  afternoon  true  night  false
CITY:  DCA =================
HIGH:  93
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  IAH =================
HIGH:  97
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  OAK =================
HIGH:  71
HOURLYS:  60
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SFO =================
HIGH:  70
HOURLYS:  59
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SJC =================
HIGH:  81
HOURLYS:  61
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ATL =================
HIGH:  94
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  BOS =================
HIGH:  92
HOURLYS:  78
LOW:  78
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  PHX =================
HIGH:  94
HOURLYS:  74
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  SEA =================
HIGH:  84
HOURLYS:  61
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SRQ =================
HIGH:  88
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  TPA =================
HIGH:  91
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  DTW =================
HIGH:  79
HOURLYS:  62
LOW:  61
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MSP =================
HIGH:  86
HOURLYS:  58
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MIA =================
HIGH:  88
HOURLYS:  81
LOW:  80
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  rainy
RAIN PERIODS:  morning  true  afternoon  true  night  true
CITY:  DIA =================
HIGH:  96
HOURLYS:  64
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  MCO =================
HIGH:  90
HOURLYS:  77
LOW:  77
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  CLE =================
HIGH:  78
HOURLYS:  70
LOW:  69
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SAC =================
HIGH:  100
HOURLYS:  63
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  STL =================
HIGH:  84
HOURLYS:  69
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PDX =================
HIGH:  93
HOURLYS:  68
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CLT =================
HIGH:  96
HOURLYS:  76
LOW:  76
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  true  night  true
CITY:  PIT =================
HIGH:  82
HOURLYS:  68
LOW:  68
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  RDU =================
HIGH:  95
HOURLYS:  76
LOW:  76
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  BWI =================
HIGH:  92
HOURLYS:  74
LOW:  73
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  BNA =================
HIGH:  91
HOURLYS:  73
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  IND =================
HIGH:  84
HOURLYS:  71
LOW:  70
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SEE =================
HIGH:  0
HOURLYS:  66
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SAN =================
HIGH:  83
HOURLYS:  71
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SAT =================
HIGH:  96
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  BDL =================
HIGH:  90
HOURLYS:  73
LOW:  73
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  true  afternoon  false  night  false
CITY:  MCI =================
HIGH:  87
HOURLYS:  66
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CMH =================
HIGH:  83
HOURLYS:  70
LOW:  70
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CVG =================
HIGH:  85
HOURLYS:  70
LOW:  69
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PBI =================
HIGH:  89
HOURLYS:  75
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  true
CITY:  AVL =================
HIGH:  89
HOURLYS:  65
LOW:  65
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  GSP =================
HIGH:  93
HOURLYS:  73
LOW:  72
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  true  night  true
CITY:  ATT =================
HIGH:  99
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  LAS =================
HIGH:  104
HOURLYS:  85
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  OKC =================
HIGH:  87
HOURLYS:  70
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  true  afternoon  false  night  false
CITY:  JAX =================
HIGH:  84
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  GRR =================
HIGH:  80
HOURLYS:  56
LOW:  55
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  BHM =================
HIGH:  89
HOURLYS:  73
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MDT =================
HIGH:  86
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ABQ =================
HIGH:  84
HOURLYS:  67
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
WINDY DAY:  true
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ORF =================
HIGH:  92
HOURLYS:  77
LOW:  77
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  true  night  true
CITY:  GSO =================
HIGH:  93
HOURLYS:  73
LOW:  73
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  SDF =================
HIGH:  86
HOURLYS:  76
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  MEM =================
HIGH:  92
HOURLYS:  73
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  NEW =================
HIGH:  94
HOURLYS:  82
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PVD =================
HIGH:  91
HOURLYS:  74
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  BUF =================
HIGH:  78
HOURLYS:  62
LOW:  62
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  FAT =================
HIGH:  102
HOURLYS:  75
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  RIC =================
HIGH:  94
HOURLYS:  73
LOW:  73
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  true
CITY:  FMY =================
HIGH:  84
HOURLYS:  75
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  AVP =================
HIGH:  80
HOURLYS:  71
LOW:  71
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  LIT =================
HIGH:  87
HOURLYS:  75
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MOB =================
HIGH:  93
HOURLYS:  74
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  true  night  false
CITY:  PNS =================
HIGH:  95
HOURLYS:  79
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  ALB =================
HIGH:  78
HOURLYS:  67
LOW:  66
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  TYS =================
HIGH:  96
HOURLYS:  73
LOW:  72
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  TUL =================
HIGH:  91
HOURLYS:  72
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  LEX =================
HIGH:  84
HOURLYS:  68
LOW:  68
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  rainy
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  DAY =================
HIGH:  83
HOURLYS:  68
LOW:  68
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  TUC =================
HIGH:  89
HOURLYS:  69
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  clear
RAIN PERIODS:  morning  true  afternoon  true  night  true
CITY:  HNL =================
HIGH:  88
HOURLYS:  76
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ICT =================
HIGH:  90
HOURLYS:  68
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  DSM =================
HIGH:  83
HOURLYS:  60
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  GRB =================
HIGH:  80
HOURLYS:  52
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  LYH =================
HIGH:  96
HOURLYS:  71
LOW:  70
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ROA =================
HIGH:  95
HOURLYS:  71
LOW:  71
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  FNT =================
HIGH:  78
HOURLYS:  56
LOW:  56
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MBS =================
HIGH:  76
HOURLYS:  55
LOW:  54
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  GEG =================
HIGH:  97
HOURLYS:  66
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CRW =================
HIGH:  86
HOURLYS:  69
LOW:  69
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  OMA =================
HIGH:  86
HOURLYS:  58
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SGF =================
HIGH:  87
HOURLYS:  71
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ROC =================
HIGH:  77
HOURLYS:  62
LOW:  62
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CAE =================
HIGH:  94
HOURLYS:  77
LOW:  76
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  TOL =================
HIGH:  81
HOURLYS:  64
LOW:  63
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PWM =================
HIGH:  90
HOURLYS:  70
LOW:  69
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  HSV =================
HIGH:  91
HOURLYS:  71
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  true
CITY:  MSN =================
HIGH:  80
HOURLYS:  51
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CGI =================
HIGH:  88
HOURLYS:  74
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  PAH =================
HIGH:  88
HOURLYS:  72
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  cloudy
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MWA =================
HIGH:  0
HOURLYS:  73
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SHV =================
HIGH:  97
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  HRL =================
HIGH:  96
HOURLYS:  75
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SYR =================
HIGH:  80
HOURLYS:  68
LOW:  67
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ACT =================
HIGH:  97
HOURLYS:  76
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  COS =================
HIGH:  90
HOURLYS:  57
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CMI =================
HIGH:  83
HOURLYS:  61
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  DEC =================
HIGH:  84
HOURLYS:  62
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SPI =================
HIGH:  84
HOURLYS:  61
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CHA =================
HIGH:  94
HOURLYS:  73
LOW:  72
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  SAV =================
HIGH:  90
HOURLYS:  76
LOW:  75
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ELP =================
HIGH:  79
HOURLYS:  66
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  rainy
NIGHT:  rainy
RAIN PERIODS:  morning  true  afternoon  true  night  false
CITY:  SJU =================
HIGH:  91
HOURLYS:  80
LOW:  80
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CID =================
HIGH:  83
HOURLYS:  52
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  ALO =================
HIGH:  84
HOURLYS:  50
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  CHS =================
HIGH:  90
HOURLYS:  75
LOW:  74
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  JAN =================
HIGH:  94
HOURLYS:  77
LOW:  999
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  clear
RAIN PERIODS:  morning  false  afternoon  false  night  false
CITY:  MYR =================
HIGH:  88
HOURLYS:  77
LOW:  77
SNOW COUNT:  0
FREEZING COUNT:  0
DAY:  sunny
NIGHT:  cloudy
RAIN PERIODS:  morning  false  afternoon  false  night  false
