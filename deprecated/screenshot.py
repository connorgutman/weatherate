from codecs import open
from network_codes import network_codes
from datetime import datetime, timedelta
from selenium import webdriver
from sites import ABC
from offset import offset

options = webdriver.ChromeOptions()
options.add_argument('headless')
browser = webdriver.Chrome(
    executable_path='/usr/bin/chromedriver', chrome_options=options)

text_file = open("/home/gwynn/Work/Weatherate/output/ABC_DATA.py", "w")
error_file = open("/home/gwynn/Work/Weatherate/output/errors.py", 'a')

print 'SCRAPING ABC ======================================='

current_date = datetime.now()
current_day = current_date.strftime("%a").lower()
print 'Today is: ', current_day

degree_sign = u'\N{DEGREE SIGN}'

weather_codes = {
    'w1': '1',
    'w2': '1',
    'w3': '1',
    'w4': '1',
    'w5': '1',
    'w6': '2',
    'w7': '2',
    'w8': '2',
    'w9': '0',
    'w10': '0',
    'w11': '2',
    'w12': '3',
    'w13': '3',
    'w14': '3',
    'w15': '10',
    'w16': '10',
    'w17': '10',
    'w18': '3',
    'w19': '4',
    'w20': '4',
    'w21': '4',
    'w22': '4',
    'w23': '4',
    'w24': '4',
    'w25': '6',
    'w26': '6',
    'w27': '0',
    'w28': '0',
    'w29': '6'
}

ABC_DATA = {}
ERROR_DATA = {}

for network, site in ABC.iteritems():
    try:
        browser.get(site)
        DOM = browser.page_source.encode('utf-8')
        print(network + ' Forecasts:')
        forecast = browser.find_element_by_class_name('hourly-sevenday')
        days = forecast.find_elements_by_class_name('CarouselSlide')

        starting_day = days[0].find_element_by_class_name(
            'day-header').get_attribute('innerHTML').lower()[:3]
        if starting_day != current_day:
            print 'Website is not up to date. First day on record is ', starting_day
            offset(days, starting_day, current_day)

        network_code = network_codes[network]
        ABC_DATA[network] = {
            'high'+ network_code + '[0]': '',
            'low'+ network_code + '[1]': '',
            'high'+ network_code + '[2]': '',
            'low'+ network_code + '[2]': '',
            'high'+ network_code + '[3]': '',
            'low'+ network_code + '[3]': '',
            'high'+ network_code + '[4]': '',
            'low'+ network_code + '[4]': '',
            'weather'+ network_code + '[0]': '',
            'weather'+ network_code + '[1]': '',
            'weather'+ network_code + '[2]': '',
            'weather'+ network_code + '[3]': '',
            'weather'+ network_code + '[4]': ''
        }

        dayNumb = 1
        for day in days:
            date = day.find_element_by_class_name(
                'day-header').get_attribute('innerHTML').lower()
            sky_code = day.find_element_by_tag_name(
                'img').get_attribute('src').split('icons/')[1].split('.')[0]
            if sky_code in weather_codes:
                sky = weather_codes[sky_code]
            else:
                sky = sky_code
            high = day.find_element_by_class_name(
                'temp').get_attribute('innerHTML').split(degree_sign)[0]
            low = day.find_element_by_class_name(
                'data').get_attribute('innerHTML').split(degree_sign)[0]
            print date, sky, high, low
            if dayNumb == 1:
                ABC_DATA[network]['high'+ network_code + '[0]'] = str(high)
                ABC_DATA[network]['low'+ network_code + '[1]'] = str(low)
                ABC_DATA[network]['weather'+ network_code + '[0]'] = str(sky)
                ABC_DATA[network]['weather'+ network_code + '[1]'] = '0'
            elif dayNumb < 5:
                ABC_DATA[network]['high'+ network_code + '[' + str(dayNumb) + ']'] = str(high)
                ABC_DATA[network]['low'+ network_code + '[' + str(dayNumb) + ']'] = str(low)
                ABC_DATA[network]['weather'+ network_code + '[' + str(dayNumb) + ']'] = str(sky)
            else:
                print 'Exceeded Day 4, skipping file entry.'
            dayNumb = dayNumb + 1
    except:
        print 'Could not fetch site ' + network
        ERROR_DATA[network] = {'error': 'could not fetch'}
        
print >> text_file, 'ABC_DATA = ', ABC_DATA
print >> error_file, 'ABC_ERRORS = ', ERROR_DATA
browser.close()
browser.quit()
text_file.close()
