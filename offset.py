def offset(days, starting_day, current_day):
    week = {
            'sun': 0,
            'mon': 1,
            'tue': 2,
            'wed': 3,
            'thu': 4,
            'fri': 5,
            'sat': 6
        }
    if week[starting_day] < week[current_day]:
        if week[starting_day] < 3 and week[current_day] > 3:
            print('Website is likely ahead, or SUPER behind.')
            # do something
        else:    
            print('Website is behind')
            off_by = week[current_day] - week[starting_day]
            print(('Off by ', off_by))
            current_offset = 0
            while current_offset < off_by:
                days.pop(0)
                current_offset = current_offset + 1

    elif week[starting_day] > week[current_day]:
        if week[current_day] < 3 and week [starting_day] > 3:
            print('Website is likely behind, or SUPER ahead.')
            off_by = 7 - week[starting_day]
            print(('Off by ', off_by))
            current_offset = 0
            while current_offset < off_by:
                days.pop(0)
                current_offset = current_offset + 1
        else:
            print('Website is ahead')
            off_by = week[current_day] - week[starting_day]
            current_offset = 0
            while current_offset < off_by:
                # Insert new placeholder day with values of 0 to days object
                print('YIKES')
                current_offset = current_offset + 1
                
    else:
        print('Website is being weird...')
        # do something

    # starting_day = days[0].find_element_by_class_name('day-header').get_attribute('innerHTML')
    # print 'New starting day: ', starting_day
