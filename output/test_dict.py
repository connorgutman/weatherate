from codecs import open
from weather_codes import weather_codes
from ABC_DATA import ABC_DATA
from AFFILIATE_1_DATA import AFFILIATE_1_DATA
from AFFILIATE_2_DATA import AFFILIATE_2_DATA
from AFFILIATE_3_DATA import AFFILIATE_3_DATA
from AFFILIATE_4_DATA import AFFILIATE_4_DATA
from AFFILIATE_5_DATA import AFFILIATE_5_DATA
from AFFILIATE_6_DATA import AFFILIATE_6_DATA
from AFFILIATE_7_DATA import AFFILIATE_7_DATA
from AFFILIATE_8_DATA import AFFILIATE_8_DATA
from AFFILIATE_9_DATA import AFFILIATE_9_DATA
from AFFILIATE_10_DATA import AFFILIATE_10_DATA
from AFFILIATE_11_DATA import AFFILIATE_11_DATA
from AFFILIATE_12_DATA import AFFILIATE_12_DATA
from CBS_DATA import CBS_DATA
from CW_DATA import CW_DATA
from ESPANOL_1_DATA import ESPANOL_1_DATA
from ESPANOL_3_DATA import ESPANOL_3_DATA
from ESPANOL_NBC_DATA import ESPANOL_NBC_DATA
from FOX_DATA import FOX_DATA
from NBC_DATA import NBC_DATA
from KSL_DATA import KSL_DATA

codes_file = open("/home/ber/Work/Weatherate/weather_codes.py", "w")
output_codes_file = open("weather_codes.py",
                         "w")
backup_codes_file = open("/home/ber/Work/Weatherate/backup_weather_codes.py",
                         "w")
print('weather_codes = ', weather_codes, file=backup_codes_file)

root_network_path = '/home/ber/Work/Weatherate/output'

network_paths = ['ABC', 'AFFILIATE_1', 'AFFILIATE_2', 'AFFILIATE_3', 'AFFILIATE_4', 'AFFILIATE_5', 'AFFILIATE_6', 'AFFILIATE_7', 'AFFILIATE_8', 'AFFILIATE_9', 'AFFILIATE_10', 'AFFILIATE_11', 'CBS', 'CW', 'ESPANOL_1', 'ESPANOL_3', 'ESPANOL_NBC', 'FOX', 'NBC', 'KSL']

new_codes = {}

network_count = 0
for network in ABC_DATA, AFFILIATE_1_DATA, AFFILIATE_2_DATA, AFFILIATE_3_DATA, AFFILIATE_4_DATA, AFFILIATE_5_DATA, AFFILIATE_6_DATA, AFFILIATE_7_DATA, AFFILIATE_8_DATA, AFFILIATE_9_DATA, AFFILIATE_10_DATA, AFFILIATE_11_DATA, CBS_DATA, CW_DATA, ESPANOL_1_DATA, ESPANOL_3_DATA, ESPANOL_NBC_DATA, FOX_DATA, NBC_DATA, KSL_DATA:
    
    network_path = root_network_path + network_paths[network_count] + '_DATA.py'
    backup_network_path = root_network_path + network_paths[network_count] + '_DATA_BACKUP.py'
    network_file = open(network_path, "w")
    backup_network_file = open(backup_network_path, "w")
    print(network_paths[network_count] + '_DATA = ', network, file=backup_network_file)

    for station in network:
        for forecast_type, forecast_value in network[station].items():
            try:
                val = int(forecast_value)
            except ValueError:
                try:
                    val = int(forecast_value.replace(",", ""))
                except:
                    if forecast_value:
                        try:
                            new_forecast = new_codes[forecast_value]
                            network[station][forecast_type] = new_forecast
                        except:
                            print(forecast_type, forecast_value)
                            wcode = input("enter forecast: ")
                            weather_codes[forecast_value] = wcode
                            new_codes[forecast_value] = wcode
                            network[station][forecast_type] = wcode
                            try:
                                print(network)
                            except:
                                print('derp')

    print(network_paths[network_count] + '_DATA = ', network, file=network_file)
    network_count = network_count + 1

print('weather_codes = ', weather_codes, file=codes_file)
print('weather_codes = ', weather_codes, file=output_codes_file)
