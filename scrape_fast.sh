#!/bin/sh
printf 'Clearing output directory...\n'
rm -rf output
mkdir output
cp INITIAL_SUBMIT.py output/INITIAL_SUBMIT.py
cp SPANISH_SUBMIT.py output/SPANISH_SUBMIT.py
cp SECONDARY_SUBMIT.py output/SECONDARY_SUBMIT.py
cp network_codes.py output/network_codes.py
cp weather_codes.py output/weather_codes.py
cp tests/test_dict.py output/test_dict.py
cp tests/test_codes.py output/test_codes.py
cp schedule.py output/schedule.py
cp city_codes.py output/city_codes.py
CURRENTDATE=$(date +%Y-%m-%d)
echo ${CURRENTDATE}
echo ${CURRENTDATE} >> output/currentdate.log
printf 'Establishing empty data files...\n'
echo "ABC_DATA={}" >> output/ABC_DATA.py
echo "AFFILIATE_1_DATA={}" >> output/AFFILIATE_1_DATA.py
echo "AFFILIATE_2_DATA={}" >> output/AFFILIATE_2_DATA.py
echo "AFFILIATE_3_DATA={}" >> output/AFFILIATE_3_DATA.py
echo "AFFILIATE_4_DATA={}" >> output/AFFILIATE_4_DATA.py
echo "AFFILIATE_5_DATA={}" >> output/AFFILIATE_5_DATA.py
echo "AFFILIATE_6_DATA={}" >> output/AFFILIATE_6_DATA.py
echo "AFFILIATE_7_DATA={}" >> output/AFFILIATE_7_DATA.py
echo "AFFILIATE_8_DATA={}" >> output/AFFILIATE_8_DATA.py
echo "AFFILIATE_9_DATA={}" >> output/AFFILIATE_9_DATA.py
echo "AFFILIATE_10_DATA={}" >> output/AFFILIATE_10_DATA.py
echo "AFFILIATE_11_DATA={}" >> output/AFFILIATE_11_DATA.py
echo "AFFILIATE_12_DATA={}" >> output/AFFILIATE_12_DATA.py
echo "CBS_DATA={}" >> output/CBS_DATA.py
echo "CW_DATA={}" >> output/CW_DATA.py
echo "ESPANOL_1_DATA={}" >> output/ESPANOL_1_DATA.py
echo "ESPANOL_3_DATA={}" >> output/ESPANOL_3_DATA.py
echo "ESPANOL_NBC_DATA={}" >> output/ESPANOL_NBC_DATA.py
echo "FOX_DATA={}" >> output/FOX_DATA.py
echo "NBC_DATA={}" >> output/NBC_DATA.py
echo "KSL_DATA={}" >> output/KSL_DATA.py
printf 'Scraping Networks...\n'
/usr/bin/python3 ABC.py &
/usr/bin/python3 AFFILIATE_1.py &
/usr/bin/python3 AFFILIATE_2.py
/usr/bin/python3 AFFILIATE_3.py &
/usr/bin/python3 AFFILIATE_4.py &
/usr/bin/python3 AFFILIATE_5.py
/usr/bin/python3 AFFILIATE_6.py &
/usr/bin/python3 AFFILIATE_7.py &
/usr/bin/python3 AFFILIATE_8.py
/usr/bin/python3 AFFILIATE_9.py &
/usr/bin/python3 AFFILIATE_10.py &
/usr/bin/python3 AFFILIATE_11.py
/usr/bin/python3 AFFILIATE_12.py &
/usr/bin/python3 CBS.py &
/usr/bin/python3 FOX.py
/usr/bin/python3 NBC.py &
/usr/bin/python3 KSL.py
printf 'Done scraping English sites!'
printf 'Scraping spansh stations...'
/usr/bin/python3 ESPANOL_1.py &
/usr/bin/python3 ESPANOL_3.py &
/usr/bin/python3 ESPANOL_NBC.py
printf 'Done scraping Spanish sites!'
printf 'Starting initial English entry...'
/usr/bin/python3 output/INITIAL_SUBMIT.py
printf 'Done entering initial submissions!'
# printf 'Starting initial Spanish entry...'
# /usr/bin/python3 /home/gwynn/Work/Weatherate/output/SPANISH_SUBMIT.py
# printf 'Starting secondary English entry...'
# /usr/bin/python3 /home/gwynn/Work/Weatherate/output/SECONDARY_SUBMIT.py