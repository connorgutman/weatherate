#!/bin/sh
printf 'Clearing output directory...\n'
rm -rf /home/ber/Work/Weatherate/output
mkdir /home/ber/Work/Weatherate/output
cp /home/ber/Work/Weatherate/INITIAL_SUBMIT.py /home/ber/Work/Weatherate/output/INITIAL_SUBMIT.py
cp /home/ber/Work/Weatherate/SPANISH_SUBMIT.py /home/ber/Work/Weatherate/output/SPANISH_SUBMIT.py
cp /home/ber/Work/Weatherate/SECONDARY_SUBMIT.py /home/ber/Work/Weatherate/output/SECONDARY_SUBMIT.py
cp /home/ber/Work/Weatherate/network_codes.py /home/ber/Work/Weatherate/output/network_codes.py
cp /home/ber/Work/Weatherate/weather_codes.py /home/ber/Work/Weatherate/output/weather_codes.py
cp /home/ber/Work/Weatherate/tests/test_dict.py /home/ber/Work/Weatherate/output/test_dict.py
cp /home/ber/Work/Weatherate/tests/test_codes.py /home/ber/Work/Weatherate/output/test_codes.py
cp /home/ber/Work/Weatherate/schedule.py /home/ber/Work/Weatherate/output/schedule.py
cp /home/ber/Work/Weatherate/city_codes.py /home/ber/Work/Weatherate/output/city_codes.py
CURRENTDATE=$(date +%Y-%m-%d)
echo ${CURRENTDATE}
echo ${CURRENTDATE} >> /home/ber/Work/Weatherate/output/currentdate.log
printf 'Establishing empty data files...\n'
echo "ABC_DATA={}" >> /home/ber/Work/Weatherate/output/ABC_DATA.py
echo "AFFILIATE_1_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_1_DATA.py
echo "AFFILIATE_2_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_2_DATA.py
echo "AFFILIATE_3_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_3_DATA.py
echo "AFFILIATE_4_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_4_DATA.py
echo "AFFILIATE_5_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_5_DATA.py
echo "AFFILIATE_6_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_6_DATA.py
echo "AFFILIATE_7_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_7_DATA.py
echo "AFFILIATE_8_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_8_DATA.py
echo "AFFILIATE_9_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_9_DATA.py
echo "AFFILIATE_10_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_10_DATA.py
echo "AFFILIATE_11_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_11_DATA.py
echo "AFFILIATE_12_DATA={}" >> /home/ber/Work/Weatherate/output/AFFILIATE_12_DATA.py
echo "CBS_DATA={}" >> /home/ber/Work/Weatherate/output/CBS_DATA.py
echo "CW_DATA={}" >> /home/ber/Work/Weatherate/output/CW_DATA.py
echo "ESPANOL_1_DATA={}" >> /home/ber/Work/Weatherate/output/ESPANOL_1_DATA.py
echo "ESPANOL_3_DATA={}" >> /home/ber/Work/Weatherate/output/ESPANOL_3_DATA.py
echo "ESPANOL_NBC_DATA={}" >> /home/ber/Work/Weatherate/output/ESPANOL_NBC_DATA.py
echo "FOX_DATA={}" >> /home/ber/Work/Weatherate/output/FOX_DATA.py
echo "NBC_DATA={}" >> /home/ber/Work/Weatherate/output/NBC_DATA.py
echo "KSL_DATA={}" >> /home/ber/Work/Weatherate/output/KSL_DATA.py
printf 'Scraping Networks...\n'
/usr/bin/python3 /home/ber/Work/Weatherate/ABC.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_1.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_2.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_3.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_4.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_5.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_6.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_7.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_8.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_9.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_10.py
/usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_11.py
# /usr/bin/python3 /home/ber/Work/Weatherate/AFFILIATE_12.py
/usr/bin/python3 /home/ber/Work/Weatherate/CBS.py
/usr/bin/python3 /home/ber/Work/Weatherate/FOX.py
/usr/bin/python3 /home/ber/Work/Weatherate/NBC.py
/usr/bin/python3 /home/ber/Work/Weatherate/KSL.py
printf 'Done scraping English sites!'
printf 'Scraping spansh stations...'
/usr/bin/python3 /home/ber/Work/Weatherate/ESPANOL_1.py
/usr/bin/python3 /home/ber/Work/Weatherate/ESPANOL_3.py
/usr/bin/python3 /home/ber/Work/Weatherate/ESPANOL_NBC.py
printf 'Done scraping Spanish sites!'
sleep 2h
printf 'Starting initial English entry...'
/usr/bin/python3 /home/ber/Work/Weatherate/output/INITIAL_SUBMIT.py
printf 'Done entering initial submissions!'
printf 'Starting initial Spanish entry...'
/usr/bin/python3 /home/ber/Work/Weatherate/output/SPANISH_SUBMIT.py
printf 'Starting secondary English entry...'
/usr/bin/python2 /home/ber/Work/Weatherate/SECONDARY_SUBMIT_P2.py
