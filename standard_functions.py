from datetime import datetime, timedelta
from weather_codes import weather_codes
from standard_data import degree_sign

"Standard Functions"
def grab(v, s, o):
    if s == 'class':
        return o.find_element_by_class_name(v)
    elif s == 'classes':
        return o.find_elements_by_class_name(v)
    elif s == 'name':
        return o.find_element_by_name(v)
    elif s == 'xpath':
        return o.find_element_by_xpath(v)
    elif s == 'id':
        return o.find_element_by_id(v)
    elif s == 'tag':
        return o.find_element_by_tag_name(v)
    elif s == 'css':
        return o.find_element_by_css_selector(v)
    else:
        pass

def strip(n, e, v):
    if n == 'ABC':
        if e == 'high'  or e == 'low':
            return v.split(degree_sign)[0]
        elif e == 'sky':
            return v.split('icons/')[1].split('.')[0]
        else:
            return v
    elif n == 'AFFILIATE_1':
        if e == 'high'  or e == 'low':
            return v.split(degree_sign)[0].strip()
        elif e == 'sky':
            return v.strip()
        else:
            return v
    elif n == 'AFFILIATE_2':
        if e == 'high'  or e == 'low':
            return v.strip()
        elif e == 'sky':
            return v.strip()
        else:
            return v
    elif n == 'AFFILIATE_3':
        if e == 'high':
            return v.split(degree_sign)[0].strip()
        elif e == 'low':
            return v.split('/')[1].split(degree_sign)[0].strip()
        elif e == 'sky':
            return v.strip()
        else:
            return v
    elif n == 'AFFILIATE_4':
        if e == 'high' or e == 'low':
            return v.split(degree_sign)[0].split('\n\t\t\t\t\t\t\t\t')[1]
        elif e == 'sky':
            return v.split('<')[0].strip()
        else:
            return v
    elif n == 'AFFILIATE_5':
        if e == 'high' or e == 'low':
            return v.split('<i')[0]
        elif e == 'sky':
            return v
        else:
            return v
    elif n == 'AFFILIATE_7':
        if e == 'high' or e == 'low':
            return v.split(degree_sign)[0]
        elif e == 'sky':
            return v.split('">')[1].strip()
        else:
            return v
    elif n == 'AFFILIATE_10':
        if e == 'high':
            return v.strip().split(degree_sign)[0]
        elif e == 'low':
            return v.split('/')[1].split(degree_sign)[0].strip()
        elif e == 'sky':
            return v.strip()
        else:
            return v
    elif n == 'AFFILIATE_11':
        if e == 'high' or e == 'low':
            return v.strip().split(degree_sign)[0]
        elif e == 'sky':
            return v.strip().split(degree_sign)[0].split('wi-')[1]
        else:
            return v
    elif n == 'CBS':
        pass
    else:
        pass

def checkDate(days, sel):
    current_day = datetime.now().strftime("%a").lower()
    print(('Today is: ', current_day))
    starting_day = grab(sel['startingDay'], 'css',
                        days[0]).get_attribute('innerHTML').strip().lower()[:3]
    if starting_day != current_day:
        print(('Website is not up to date. First day on record is ', starting_day))
        offset(days, starting_day, current_day)
    else:
        print('Website is up to date.')

def recordEntry(BEF_DATA, network, dayNumb, sky, high, low):
    if dayNumb == 1:
        BEF_DATA[network]['high[0]'] = str(high)
        BEF_DATA[network]['low[1]'] = str(low)
        BEF_DATA[network]['weather[0]'] = str(sky)
        BEF_DATA[network]['weather[1]'] = '0'
        print((dayNumb, sky, high, low))
    elif dayNumb < 5:
        BEF_DATA[network]['high' + '[' + str(dayNumb) + ']'] = str(high)
        BEF_DATA[network]['low' + '[' + str(dayNumb) + ']'] = str(low)
        BEF_DATA[network]['weather' + '[' + str(dayNumb) + ']'] = str(sky)
        print((dayNumb, sky, high, low))
    else:
        pass

def checkDict(sky_data):
    if sky_data in weather_codes:
        return weather_codes[sky_data]
    else:
        return sky_data

def offset(days, starting_day, current_day):
    week = {
            'sun': 0,
            'mon': 1,
            'tue': 2,
            'wed': 3,
            'thu': 4,
            'fri': 5,
            'sat': 6
        }
    if week[starting_day] < week[current_day]:
        if week[starting_day] < 3 and week[current_day] > 3:
            print('Website is likely ahead, or SUPER behind.')
            # do something
        else:    
            print('Website is behind')
            off_by = week[current_day] - week[starting_day]
            print(('Off by ', off_by))
            current_offset = 0
            while current_offset < off_by:
                days.pop(0)
                current_offset = current_offset + 1

    elif week[starting_day] > week[current_day]:
        if week[current_day] < 3 and week [starting_day] > 3:
            print('Website is likely behind, or SUPER ahead.')
            off_by = 7 - week[starting_day]
            print(('Off by ', off_by))
            current_offset = 0
            while current_offset < off_by:
                days.pop(0)
                current_offset = current_offset + 1
        else:
            print('Website is ahead')
            off_by = week[current_day] - week[starting_day]
            current_offset = 0
            while current_offset < off_by:
                # Insert new placeholder day with values of 0 to days object
                print('YIKES')
                current_offset = current_offset + 1
                
    else:
        print('Website is being weird...')
        # do something

    # starting_day = days[0].find_element_by_class_name('day-header').get_attribute('innerHTML')
    # print 'New starting day: ', starting_day
