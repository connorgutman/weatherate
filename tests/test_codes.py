from codecs import open
from datetime import datetime, time, timedelta
from selenium import webdriver
from network_codes import network_codes

browser = webdriver.Chrome(
    executable_path='/usr/bin/chromedriver')

browser.get(
    'http://connorg:rockledge@weatherate.com/admin/forecast_entry_page1.php')

for network, code in network_codes.iteritems():
    
    testField = 'high' + code + '[0]'
    print network + ' / ' + code
    box = browser.find_element_by_name(testField)
    box.send_keys(network)

    comparisonField = 'stations' + code
    box2 = browser.find_element_by_name(comparisonField)
    actualNetwork = box2.get_attribute('value')

    resultsField = 'low' + code + '[1]'
    box3 = browser.find_element_by_name(resultsField)

    if network == actualNetwork:
        box3.send_keys('MATCH')
        print 'MATCH'
    else:
        box3.send_keys('NO')
        print 'DOES NOT MATCH OR SAN FRANCISCO'


# browser.close()
# browser.quit()
