from datetime import datetime, time, timedelta

from selenium import webdriver

from network_codes import network_codes
from city_codes import city_codes

options = webdriver.ChromeOptions()
# options.add_argument('headless')
browser = webdriver.Chrome(
    executable_path='/usr/bin/chromedriver', options=options)

browser.get(
    'http://weatherate.com/forecast_bulk_entry/1/2021/2/1/')

# Sign in to admin portal
unameBox = browser.find_element_by_name('username')
passBox = browser.find_element_by_name('password')
submitBox = browser.find_element_by_xpath("//button[@type='submit']")
unameBox.send_keys('connorg')
passBox.send_keys('TokyoGenso21')
submitBox.click()

# Get latest form names
stationNums = {}
for x in range(0, 356):
    ename = "form-" + str(x) + "-station_name"
    stationNameBox = browser.find_element_by_name(ename)
    stationNums[str(stationNameBox.get_attribute("value"))] = str(x)
    print(("Station " + str(x) + " is " + stationNameBox.get_attribute("value")))